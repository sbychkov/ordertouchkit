/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PriceListElement.findAll", query = "SELECT p FROM PriceListElement p"),
    @NamedQuery(name = "PriceListElement.findById", query = "SELECT p FROM PriceListElement p WHERE p.id = :id"),
    @NamedQuery(name = "PriceListElement.findByPrice", query = "SELECT p FROM PriceListElement p WHERE p.price = :price")})
public class PriceListElement implements Serializable {

    private static final long serialVersionUID = 2L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
 
    @Column(precision = 38, scale = 2)
    private BigDecimal price;
    @Column(precision = 38, scale = 3)
    private BigDecimal quantity;
    @JoinColumn(name = "PRICELIST", referencedColumnName = "ID")
    @ManyToOne
    private PriceList priceList;
    @JoinColumn(name = "ITEM", referencedColumnName = "ID")
    @ManyToOne
    private Item item;

    public PriceListElement() {
    }

    public PriceListElement(BigDecimal price, BigDecimal quantity, PriceList priceList, Item item) {
        this.price = price;
        this.quantity = quantity;
        this.priceList = priceList;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the priceList
     */
    public PriceList getPriceList() {
        return priceList;
    }

    /**
     * @param priceList the priceList to set
     */
    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }

    @Override
    public String toString() {
        return "PriceListElement{" + "id=" + id + ", price=" + price + ", quantity=" + quantity + ", priceList=" + priceList + ", item=" + item + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.price != null ? this.price.hashCode() : 0);
        hash = 43 * hash + (this.quantity != null ? this.quantity.hashCode() : 0);
        hash = 43 * hash + (this.priceList != null ? this.priceList.hashCode() : 0);
        hash = 43 * hash + (this.item != null ? this.item.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PriceListElement other = (PriceListElement) obj;
        if (this.price != other.price && (this.price == null || !this.price.equals(other.price))) {
            return false;
        }
        if (this.quantity != other.quantity && (this.quantity == null || !this.quantity.equals(other.quantity))) {
            return false;
        }
        if (this.priceList != other.priceList && (this.priceList == null || !this.priceList.equals(other.priceList))) {
            return false;
        }
        if (this.item != other.item && (this.item == null || !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

}
