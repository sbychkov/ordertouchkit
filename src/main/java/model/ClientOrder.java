/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientOrder.findAll", query = "SELECT c FROM ClientOrder c"),
    @NamedQuery(name = "ClientOrder.findById", query = "SELECT c FROM ClientOrder c WHERE c.id = :id"),
    @NamedQuery(name = "ClientOrder.findByCreatedate", query = "SELECT c FROM ClientOrder c WHERE c.createDate = :createdate"),
    @NamedQuery(name = "ClientOrder.findByEstimatedate", query = "SELECT c FROM ClientOrder c WHERE c.estimateDate = :estimatedate")})
public class ClientOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @Temporal(TemporalType.DATE)
    private Date estimateDate;

    @JoinColumn(name = "USER", referencedColumnName = "ID")
    @ManyToOne
    private User user;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne
    private Client client;
    @OneToMany(mappedBy = "clientOrder", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<OrderElement> elements;

    private boolean uploaded;
    private boolean executed;
    private BigDecimal orderSum;
    private String docNum;

    public ClientOrder() {
        elements = new ArrayList<OrderElement>();
    }

    public ClientOrder(Date createDate, Date estimateDate, User user, Client client) {
        this();
        this.createDate = createDate;
        this.estimateDate = estimateDate;
        this.user = user;
        this.client = client;
    }

    public ClientOrder(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createdate) {
        this.createDate = createdate;
    }

    public Date getEstimateDate() {
        return estimateDate;
    }

    public void setEstimateDate(Date estimateDate) {
        this.estimateDate = estimateDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<OrderElement> getElements() {
        return elements;
    }

    public void setElements(List<OrderElement> elements) {
        this.elements = elements;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.createDate != null ? this.createDate.hashCode() : 0);
        hash = 97 * hash + (this.estimateDate != null ? this.estimateDate.hashCode() : 0);
        hash = 97 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 97 * hash + (this.client != null ? this.client.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientOrder other = (ClientOrder) obj;
        if (this.createDate != other.createDate && (this.createDate == null || !this.createDate.equals(other.createDate))) {
            return false;
        }
        if (this.estimateDate != other.estimateDate && (this.estimateDate == null || !this.estimateDate.equals(other.estimateDate))) {
            return false;
        }
        if (this.user != other.user && (this.user == null || !this.user.equals(other.user))) {
            return false;
        }
        if (this.client != other.client && (this.client == null || !this.client.equals(other.client))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClientOrder{" + "id=" + id + ", createDate=" + createDate + ", estimateDate=" + estimateDate + ", user=" + user + ", client=" + client + '}';
    }

    public BigDecimal getSum() {
        BigDecimal result = BigDecimal.ZERO;

        for (OrderElement oe : elements) {
            if (oe.getElementSum() != null) {
                result.add(oe.getElementSum());
            }
        }
        this.orderSum = result;
        return result;
    }

    /**
     * @return the uploaded
     */
    public boolean isUploaded() {
        return uploaded;
    }

    /**
     * @param uploaded the uploaded to set
     */
    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    /**
     * @return the executed
     */
    public boolean isExecuted() {
        return executed;
    }

    /**
     * @param executed the executed to set
     */
    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    /**
     * @return the orderSum
     */
    public BigDecimal getOrderSum() {
        return orderSum;
    }

    /**
     * @param orderSum the orderSum to set
     */
    public void setOrderSum(BigDecimal orderSum) {
        this.orderSum = orderSum;
    }

    /**
     * @return the docNum
     */
    public String getDocNum() {
        return docNum;
    }

    /**
     * @param docNum the docNum to set
     */
    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

}
