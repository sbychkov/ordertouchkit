/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StockState.findAll", query = "SELECT s FROM StockState s"),
    @NamedQuery(name = "StockState.findById", query = "SELECT s FROM StockState s WHERE s.id = :id")})
public class StockState implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)

    @Column(nullable = false)
    private Long id;
    @OneToMany(mappedBy = "stockstate")
    private List<StockStateElement> remain;

    public StockState() {
    }

    public StockState(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    public List<StockStateElement> getRemain() {
        return Collections.unmodifiableList(remain);
    }

    public void setRemain(List<StockStateElement> stockstateelementList) {
        this.remain = stockstateelementList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.remain != null ? this.remain.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StockState other = (StockState) obj;
        if (this.remain != other.remain && (this.remain == null || !this.remain.equals(other.remain))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StockState{" + "id=" + id + '}';
    }
    
}
