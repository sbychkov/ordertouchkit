/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StockStateElement.findAll", query = "SELECT s FROM StockStateElement s"),
    @NamedQuery(name = "StockStateElement.findById", query = "SELECT s FROM StockStateElement s WHERE s.id = :id"),
    @NamedQuery(name = "StockStateElement.findByQuantity", query = "SELECT s FROM StockStateElement s WHERE s.quantity = :quantity")})
public class StockStateElement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)

    @Column(nullable = false)
    private Long id;
    private Integer quantity;
    @JoinColumn(name = "ITEM", referencedColumnName = "ID")
    @ManyToOne
    private Item item;
    @JoinColumn(name = "STOCKSTATE", referencedColumnName = "ID")
    @ManyToOne
    private StockState stockstate;

    public StockStateElement() {
    }

    public StockStateElement(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public StockState getStockstate() {
        return stockstate;
    }

    public void setStockstate(StockState stockstate) {
        this.stockstate = stockstate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.quantity != null ? this.quantity.hashCode() : 0);
        hash = 23 * hash + (this.item != null ? this.item.hashCode() : 0);
        hash = 23 * hash + (this.stockstate != null ? this.stockstate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StockStateElement other = (StockStateElement) obj;
        if (this.quantity != other.quantity && (this.quantity == null || !this.quantity.equals(other.quantity))) {
            return false;
        }
        if (this.item != other.item && (this.item == null || !this.item.equals(other.item))) {
            return false;
        }
        if (this.stockstate != other.stockstate && (this.stockstate == null || !this.stockstate.equals(other.stockstate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StockStateElement{" + "id=" + id + ", quantity=" + quantity + ", item=" + item + ", stockstate=" + stockstate + '}';
    }
    
}
