/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PriceList.findAll", query = "SELECT p FROM PriceList p"),
    @NamedQuery(name = "PriceList.findById", query = "SELECT p FROM PriceList p WHERE p.id = :id"),
    @NamedQuery(name = "PriceList.findByCreateDate", query = "SELECT p FROM PriceList p WHERE p.createDate = :createdate")})
public class PriceList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date createDate;
    @OneToMany(mappedBy = "priceList",cascade = CascadeType.ALL)
    private List<PriceListElement> elements;

    public PriceList() {
    }

    public PriceList(Date createDate) {
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @XmlTransient
    public List<PriceListElement> getElements() {
        return elements;
    }

    public void setElements(List<PriceListElement> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "PriceList{" + "id=" + id + ", createDate=" + createDate + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.createDate != null ? this.createDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PriceList other = (PriceList) obj;
        if (this.createDate != other.createDate && (this.createDate == null || !this.createDate.equals(other.createDate))) {
            return false;
        }
        return true;
    }

}
