/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author 1
 */
@Entity
public class DebtDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne
    private Client client;
    private String name;
    private String type;
    private BigDecimal debtValue;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date docDate;

    public DebtDoc() {
    }

    public DebtDoc(Client client, String name, String type,
            BigDecimal debtValue, Date docDate) {
        this.client = client;
        this.name = name;
        this.type = type;
        this.debtValue = debtValue;
        this.docDate = docDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the debtValue
     */
    public BigDecimal getDebtValue() {
        return debtValue;
    }

    /**
     * @param debtValue the debtValue to set
     */
    public void setDebtValue(BigDecimal debtValue) {
        this.debtValue = debtValue;
    }

    /**
     * @return the docDate
     */
    public Date getDocDate() {
        return docDate;
    }

    /**
     * @param docDate the docDate to set
     */
    public void setDocDate(Date docDate) {
        this.docDate = docDate;
    }

    @Override
    public String toString() {
        return "DebtDoc{" + "id=" + id + ", client=" + client + ", name=" + name
                + ", type=" + type + ", debtValue=" + debtValue + ", docDate="
                + docDate + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (this.client != null ? this.client.hashCode() : 0);
        hash = 71 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 71 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 71 * hash + (this.debtValue != null ? this.debtValue.hashCode() : 0);
        hash = 71 * hash + (this.docDate != null ? this.docDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DebtDoc other = (DebtDoc) obj;
        if (this.client != other.client && (this.client == null || !this.client.equals(other.client))) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.type == null) ? (other.type != null) : !this.type.equals(other.type)) {
            return false;
        }
        if (this.debtValue != other.debtValue && (this.debtValue == null || !this.debtValue.equals(other.debtValue))) {
            return false;
        }
        if (this.docDate != other.docDate && (this.docDate == null || !this.docDate.equals(other.docDate))) {
            return false;
        }
        return true;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

}
