/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author serge
 */
@Entity

public class UserSettings implements Serializable{

    @Id
    private Long id;
    @ManyToOne
    private User user;
    private boolean priceViewTree = false;
    private boolean priceSelectList = false;

    public UserSettings() {
    }

    
    
    public UserSettings(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserSettings{" + "id=" + id + ", user=" + user + ", priceViewTree=" + priceViewTree + ", priceSelectList=" + priceSelectList + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 29 * hash + (this.priceViewTree ? 1 : 0);
        hash = 29 * hash + (this.priceSelectList ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserSettings other = (UserSettings) obj;
        if (this.user != other.user && (this.user == null || !this.user.equals(other.user))) {
            return false;
        }
        if (this.priceViewTree != other.priceViewTree) {
            return false;
        }
        if (this.priceSelectList != other.priceSelectList) {
            return false;
        }
        return true;
    }

    

    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the priceViewTree
     */
    public boolean isPriceViewTree() {
        return priceViewTree;
    }

    /**
     * @param priceViewTree the priceViewTree to set
     */
    public void setPriceViewTree(boolean priceViewTree) {
        this.priceViewTree = priceViewTree;
    }

    /**
     * @return the priceSelectList
     */
    public boolean isPriceSelectList() {
        return priceSelectList;
    }

    /**
     * @param priceSelectList the priceSelectList to set
     */
    public void setPriceSelectList(boolean priceSelectList) {
        this.priceSelectList = priceSelectList;
    }

}
