/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderElement.findAll", query = "SELECT o FROM OrderElement o"),
    @NamedQuery(name = "OrderElement.findById", query = "SELECT o FROM OrderElement o WHERE o.id = :id"),
    @NamedQuery(name = "OrderElement.findByQuantity", query = "SELECT o FROM OrderElement o WHERE o.quantity = :quantity")})
public class OrderElement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private Integer quantity;
    @JoinColumn(name = "ITEM", referencedColumnName = "ID")
    @ManyToOne
    private Item item;
    @JoinColumn(name = "CLIENTORDER", referencedColumnName = "ID")    
    private ClientOrder clientOrder;
    private BigDecimal elementSum=BigDecimal.ZERO;
    private BigDecimal elementPrice;
    
    public OrderElement() {
    }

    public OrderElement(Integer quantity, Item item, ClientOrder clientOrder) {
        this.quantity = quantity;
        this.item = item;
        this.clientOrder = clientOrder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public ClientOrder getClientOrder() {
        return clientOrder;
    }

    public void setClientOrder(ClientOrder clientOrder) {
        this.clientOrder = clientOrder;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.quantity != null ? this.quantity.hashCode() : 0);
        hash = 67 * hash + (this.item != null ? this.item.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderElement other = (OrderElement) obj;
        if (this.quantity != other.quantity && (this.quantity == null || !this.quantity.equals(other.quantity))) {
            return false;
        }
        if (this.item != other.item && (this.item == null || !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OrderElement{" + "id=" + id + ", quantity=" + quantity + ", item=" + item + ", clientOrder=" + clientOrder + '}';
    }

    /**
     * @return the elementSum
     */
    public BigDecimal getElementSum() {
        return elementSum;
    }

    /**
     * @param elementSum the elementSum to set
     */
    public void setElementSum(BigDecimal elementSum) {
        this.elementSum = elementSum;
    }

    /**
     * @return the elementPrice
     */
    public BigDecimal getElementPrice() {
        return elementPrice;
    }

    /**
     * @param elementPrice the elementPrice to set
     */
    public void setElementPrice(BigDecimal elementPrice) {
        this.elementPrice = elementPrice;
    }

}
