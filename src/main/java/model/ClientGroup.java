/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author serge
 */
@Entity
public class ClientGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Integer code;
    @ManyToOne
    private ClientGroup parent;
    @OneToMany(mappedBy = "parent")
    private List<ClientGroup> children;

    public ClientGroup() {
    }

    
    public ClientGroup(String name, Integer code) {
      this.name=name;
      this.code=code;
    }

    public ClientGroup(String name, Integer code, ClientGroup parent) {
        this.name = name;
        this.code = code;
        this.parent = parent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the parent
     */
    public ClientGroup getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(ClientGroup parent) {
        this.parent = parent;
    }

    /**
     * @return the children
     */
    public List<ClientGroup> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(List<ClientGroup> children) {
        this.children = children;
    }

}
