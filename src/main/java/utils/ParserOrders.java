/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.util.filter.Compare;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ClientOrder;
import model.OrderElement;
import orderstouchkit.ctrl.Containers;

/**
 *
 * @author serge
 */
public class ParserOrders extends ParserConfig {

    private static final Logger LOG = Logger.getLogger(ParserOrders.class.getName());

    public static String in(BufferedReader br) throws IOException {
        String err = null;
        String str = null;
        String[] data = null;
        int counter = 0;
        try {
            List<String> strings = new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                strings.add(str);
            }
            LOG.info(strings.size() + " strings was read");
            for (Iterator<String> it = strings.iterator(); it.hasNext();) {
                str = it.next();
                counter++;

                data = str.split(dl, -1);
                ClientOrder co = (ClientOrder) DataCtrl.getObject(ClientOrder.class,
                        Long.valueOf(data[0]));
                if (co != null) {
                    co.setUploaded(true);
                    co.setDocNum(data[1]);
                    DataCtrl.checkObject(co);
                    LOG.info("Confirmation for order " + co + " received");
                }
            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;

            LOG.log(Level.SEVERE, nfe.getMessage(), nfe);
            LOG.severe(Arrays.toString(data));
            return err;
        }
        LOG.info("Done");
        return err;
    }

    private static String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    public static void out(PrintWriter out) {
        try {
            dl = "|";
            JPAContainer<ClientOrder> orders = Containers.getOrders();
            orders.addContainerFilter(new Compare.Equal("uploaded", false));

            for (Object itemId : orders.getItemIds()) {
                ClientOrder entity = orders.getItem(itemId).getEntity();
                StringBuilder oHeader = new StringBuilder();
                if (entity.getUser() != null) {
                    oHeader.append(entity.getUser().getName()).append(dl);
                }
                oHeader.append(dl).append(entity.getId()).append(dl);

                if (entity.getClient() != null) {
                    oHeader.append(int2code(entity.getClient().getCode(), 8))
                            .append(dl).append(entity.getClient().getName());
                }
                oHeader.append(dl).append(date2string(entity.getCreateDate())).
                        append(dl);
                if (entity.getEstimateDate() != null) {
                    oHeader.append(date2string(entity.getEstimateDate()));
                }
                oHeader.append(dl);
                for (OrderElement el : entity.getElements()) {
                    StringBuilder e = new StringBuilder(oHeader);
                    e.append(el.getItem().getCode()).
                            append(dl).append(el.getItem().getName()).
                            append(dl).append(el.getQuantity().toString()).
                            append(dl);
                    out.println(e.toString());
                }
                LOG.info("Order " + oHeader + " downloaded");
            }

        } finally {
            out.close();
        }
        LOG.info("Done");
    }

    private static String date2string(Date date) {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(date);
    }

    private static String datetime2string(Date datetime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm:ss");
        return sdf.format(datetime);
    }

    private static String int2code(int i, int len) {
        String format = "%0" + len + "d";
        return String.format(format, i);
    }
}
