/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.math.BigDecimal;
import static java.math.BigDecimal.ZERO;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import static javax.persistence.Persistence.createEntityManagerFactory;
import javax.persistence.Query;
import model.Category;
import model.Item;
import model.PriceList;
import model.PriceListElement;

/**
 *
 * @author serge
 */
public class DataCtrl {

    private static final Logger LOG = Logger.getLogger(DataCtrl.class.getName());

    public static void persist(Object o) {
        getEm().persist(o);
    }

    public static Object checkObject(Object o) {

        EntityManager em = getEm();
        Object result;
        em.getTransaction().begin();
        result = em.merge(o);
        em.getTransaction().commit();
        return result;
    }

    public static Object getObject(Class c, Object id) {
        EntityManager em = getEm();
        Object result;
        result = em.find(c, id);
        return result;
    }

    public static void persistList(List l) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        for (Object o : l) {
            em.merge(o);
        }
        em.flush();
        em.getTransaction().commit();
    }

    private static EntityManager getEm() {
        return i().getEmf().createEntityManager();
    }

    public static BigDecimal getCurrentPrice(Item item) {
        BigDecimal result = ZERO;
        EntityManager em = getEm();
        Query q = em.createQuery("Select p.price from PriceListElement p where p.item=:item order by p.priceList.createDate desc");
        q.setParameter("item", item);
        q.setMaxResults(1);
        List resultList = q.getResultList();
        if (resultList != null && !resultList.isEmpty()) {
            result = (BigDecimal) resultList.get(0);
        }
        return result;
    }

    public static Object findByCode(Class c, Integer code) {
        Object result = null;
        EntityManager em = getEm();
        Query q = em.createQuery("select o from " + c.getSimpleName() + " o where o.code =:code");
        q.setParameter("code", code);
        List resultList = q.getResultList();
        if (resultList != null && !resultList.isEmpty()) {
            result = resultList.get(0);
        }
        return result;
    }

    public static List<Category> getSubCategory(Category parent) {
        EntityManager em = getEm();

        Query q;
        if (parent == null) {
            q = em.createQuery("select c from Category c where c.parent is null");
        } else {
            q = em.createQuery("select c from Category c where c.parent =:parent");
            q.setParameter("parent", parent);
        }
        return q.getResultList();
    }

    public static List<Item> getItemByCategory(Category parent) {
        EntityManager em = getEm();
        Query q = em.createQuery("select i from Item i where i.category =:parent");
        q.setParameter("parent", parent);
        return q.getResultList();
    }

    public static PriceList getLatestPriceList() {
        PriceList result = null;
        EntityManager em = getEm();
        Query q = em.createQuery("select p from PriceList p order by p.createDate");// in (select max(pp.createDate) from PriceList pp)");
        //q.setMaxResults(1);
        List l = q.getResultList();
        /*if (l != null && l.size() > 0) {
         result = (PriceList) l.get(0);

         }*/
        for (Iterator<PriceList> it = (Iterator<PriceList>) l.iterator(); it.hasNext()
                & (result == null || result.getElements().isEmpty());) {
            result = (PriceList) it.next();
        }
        return result;
    }

    public static PriceListElement getPriceListElement(Item item) {
        PriceListElement result = null;
        EntityManager em = getEm();
        PriceList pl = getLatestPriceList();
        Query q = em.createQuery("Select p from PriceListElement p where p.item=:item and p.priceList=:pl");
        q.setParameter("item", item);
        q.setParameter("pl", pl);
        q.setMaxResults(1);
        List resultList = q.getResultList();
        if (resultList != null && !resultList.isEmpty()) {
            result = (PriceListElement) resultList.get(0);
        }
        return result;
    }

    public static DataCtrl i() {
        return single.instance;

    }
    private final EntityManagerFactory emf;

    private DataCtrl() {

        emf = createEntityManagerFactory("PU");
    }

    private EntityManagerFactory getEmf() {
        return emf;
    }

    private static class single {

        static final DataCtrl instance = new DataCtrl();

        private single() {
        }

    }

}
