/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import model.Client;
import model.ClientGroup;

/**
 *
 * @author 1
 */
public class ParserClient extends ParserConfig {

    private static final Logger LOG = Logger.getLogger(ParserClient.class.getName());

    public static String parse(BufferedReader br) throws IOException {
        String err = null;
        String str = null;
        ArrayList<Client> cls = new ArrayList<Client>();

        int counter = 0;
        try {
            List<String> strings= new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                strings.add(str);
            }
            LOG.info(strings.size() +" strings was read");
            for (Iterator<String> it = strings.iterator(); it.hasNext();) {
                str = it.next();
                String[] data = str.split(dl, -1);
                counter++;
                ClientGroup parent = null;
                if (!data[5].isEmpty()) {
                    parent = (ClientGroup) DataCtrl.findByCode(ClientGroup.class, Integer.valueOf(data[5]));
                    if (parent == null) {
                        parent = new ClientGroup(data[4], Integer.valueOf(data[5]));
                        parent = (ClientGroup) DataCtrl.checkObject(parent);
                    }
                }
                if (data[0].equals("1")) {
                    ClientGroup group = (ClientGroup) DataCtrl.findByCode(ClientGroup.class, Integer.valueOf(data[1]));
                    if (group == null) {
                        group = new ClientGroup(data[2], Integer.valueOf(data[1]), parent);
                    } else {
                        group.setName(data[2]);
                        group.setCode(Integer.valueOf(data[1]));
                        group.setParent(parent);
                    }
                    DataCtrl.checkObject(group);
                } else {

                    Client c;
                    c = (Client) DataCtrl.findByCode(Client.class, Integer.valueOf(data[1]));
                    if (c == null) {
                        c = new Client();
                    }
                    c.setCode(Integer.valueOf(data[1]));
                    c.setName(data[2]);
                    c.setDebt(new BigDecimal(replaceCommaAndSpace(data[3])));
                    c.setParent(parent);
                    cls.add(c);
                }
            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            return err;
        }

        LOG.info("Strings in post message " + counter);
        LOG.info("Objects to be persisted " + cls.size());
        DataCtrl.persistList(cls);

        /*for (Client cl : cls) {
         DataCtrl.checkObject(cl);
         }*/
        LOG.info("Done");
        return err;
    }

    private static String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    public static String parseGroup(BufferedReader br) throws IOException {
        String err = null;
        String str = null;
        int counter = 0;
        try {
            while ((str = br.readLine()) != null) {
                String[] data = str.split(dl, -1);
                counter++;
                ClientGroup parent = null;
                if (!data[5].isEmpty()) {
                    parent = (ClientGroup) DataCtrl.findByCode(ClientGroup.class, Integer.valueOf(data[5]));
                    if (parent == null) {
                        parent = new ClientGroup(data[4], Integer.valueOf(data[5]));
                        parent = (ClientGroup) DataCtrl.checkObject(parent);
                    }
                }

                if (data[0].equals("1")) {
                    ClientGroup group = (ClientGroup) DataCtrl.findByCode(ClientGroup.class, Integer.valueOf(data[1]));
                    if (group == null) {
                        group = new ClientGroup(data[2], Integer.valueOf(data[1]), parent);
                    } else {
                        group.setName(data[2]);
                        group.setCode(Integer.valueOf(data[1]));
                        group.setParent(parent);
                    }
                    DataCtrl.checkObject(group);
                }
            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            return err;
        }

        LOG.info("Strings in post message " + counter);

        return err;

    }

    private ParserClient() {
    }
}
