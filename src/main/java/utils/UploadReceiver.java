/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import com.vaadin.ui.Upload;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;
import orderstouchkit.ordersFallbackUI;
import utils.Parser;
import utils.XLSPriceList;


public class UploadReceiver implements Upload.Receiver, Upload.SucceededListener,Upload.FailedListener {
    private static final long serialVersionUID = 1L;
    private File tempFile;
    
    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {
        OutputStream result = null;
        try {
            tempFile = File.createTempFile(filename, "tmp");
            tempFile.deleteOnExit();
            result = new FileOutputStream(tempFile);
        } catch (IOException e) {
            e.printStackTrace();
        }         
        return result;
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event) {
        XLSPriceList xpl = new XLSPriceList();
        Parser p = new Parser(xpl);
        try {
            p.getInputStream(new FileInputStream(tempFile));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ordersFallbackUI.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
    }

    @Override
    public void uploadFailed(Upload.FailedEvent event) {
     event.getReason().printStackTrace();
    }
    
}
