/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Item;
import model.PriceList;
import model.PriceListElement;

/**
 *
 * @author serge
 */
public class ParserPrice extends ParserConfig {
    
    private static final Logger LOG = Logger.getLogger(ParserPrice.class.getName());
    
    public static String parse(BufferedReader br) throws IOException {
        String err = null;
        String str = null;
        String[] data = null;
        ArrayList<PriceListElement> ples = new ArrayList<PriceListElement>();
        PriceList pl = new PriceList();
        pl.setCreateDate(new Date());
        PriceList pl1 = DataCtrl.getLatestPriceList();
        if (pl1 != null && date2string(pl.getCreateDate()).equals(date2string(pl1.getCreateDate()))) {
            pl = pl1;
        }
        pl = (PriceList) DataCtrl.checkObject(pl);
        
        int counter = 0;
        
        try {
            List<String> strings = new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                strings.add(str);
            }
            LOG.info(strings.size() + " strings was read");
            for (Iterator<String> it = strings.iterator(); it.hasNext();) {
                str = it.next();
                counter++;
                
                data = str.split(dl, -1);
                if (data.length == 1) {
                    data = str.split("|", -1);
                }
                
                Item item = (Item) DataCtrl.findByCode(Item.class,
                        Integer.parseInt(data[0]));
                if (item != null) {
                    PriceListElement ple;
                    ple = DataCtrl.getPriceListElement(item);
                    if (ple == null || !ple.getPriceList().equals(pl)) {
                        ple = new PriceListElement();
                        ple.setItem(item);
                        ple.setPriceList(pl);
                    }
                    ple.setPrice(new BigDecimal(replaceCommaAndSpace(data[1])));
                    ple.setQuantity(new BigDecimal(replaceCommaAndSpace(data[2])));
                    ples.add(ple);
                    // DataCtrl.checkObject(ple);
                }
                
            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            //LOG.fine(nfe.getMessage());
            LOG.log(Level.SEVERE, nfe.getMessage(), nfe);
            LOG.severe(Arrays.toString(data));
            LOG.severe(dl);
            return err;
        }
        
        LOG.info("Strings in post message " + counter);
        LOG.info("Objects to be persisted " + ples.size());
        DataCtrl.persistList(ples);
        LOG.info("Done");
        return err;
    }
    
    private static String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }
    
    private static String date2string(Date date) {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(date);
    }
    
}
