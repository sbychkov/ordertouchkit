/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import model.Client;
import model.DebtDoc;

/**
 *
 * @author 1
 */
public class ParserDebt extends ParserConfig {

    private static final Logger LOG = Logger.getLogger(ParserDebt.class.getName());

    public static String parse(BufferedReader br) throws IOException {
        String err = null;
        String str = null;
        ArrayList<DebtDoc> dds = new ArrayList<DebtDoc>();

        int counter = 0;
        try {
            List<String> strings = new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                strings.add(str);
            }
            LOG.info(strings.size() +" strings was read");
            for (Iterator<String> it = strings.iterator(); it.hasNext();) {
                str = it.next();
                counter++;

                String[] data = str.split(dl, -1);
                Client c;
                DebtDoc dd;
                c = (Client) DataCtrl.findByCode(Client.class, Integer.valueOf(data[0]));
                if (c != null) {
                    dd = new DebtDoc();
                    dd.setClient(c);
                    dd.setName(data[1]);
                    dd.setType(data[2]);
                    dd.setDebtValue(new BigDecimal(replaceCommaAndSpace(data[3])));
                    dd.setDocDate(new Date(data[4]));
                    dds.add(dd);
                }

            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            return err;
        }

        LOG.info("Strings in post message " + counter);
        LOG.info("Objects to be persisted " + dds.size());
        DataCtrl.persistList(dds);
        LOG.info("Done");

        return err;
    }

    private static String replaceCommaAndSpace(String str) {
        String result = "0";
        if (str != null && !str.trim().isEmpty()) {
            result = str.replace(",", ".");
        }
        return result;
    }

    private ParserDebt() {
    }
}
