/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import model.Category;
import model.Item;

/**
 *
 * @author serge
 */
public class ParserStructure extends ParserConfig {

    /**
     *
     * @param reader
     * @return
     * @throws IOException
     */
    public static String parse(BufferedReader br) throws IOException {
        String str = null;
        String err = null;
        int counter = 0;
        ArrayList<Item> items = new ArrayList<Item>();
        try {
            List<String> strings = new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                strings.add(str);
            }
            LOG.info(strings.size() +" strings was read");
            for (Iterator<String> it = strings.iterator(); it.hasNext();) {
                str = it.next();
                String[] data = str.split(dl, -1);
                ++counter;
                Category parent = null;
                if (!data[4].isEmpty()) {
                    parent = (Category) DataCtrl.findByCode(Category.class, Integer.valueOf(data[4]));
                    if (parent == null) {
                        parent = new Category(data[3], Integer.valueOf(data[4]));
                        parent = (Category) DataCtrl.checkObject(parent);
                    }
                }

                if (data[0].equals("1")) {

                    Category category = (Category) DataCtrl.findByCode(Category.class, Integer.valueOf(data[2]));
                    if (category == null) {
                        category = new Category(data[1], Integer.valueOf(data[2]), parent);
                    } else {
                        category.setName(data[1]);
                        category.setCode(Integer.valueOf(data[2]));
                        category.setParent(parent);
                    }
                  //  LOG.finest(str);
                    //  LOG.finest(category.toString());
                    DataCtrl.checkObject(category);
                } else {

                    Item item = (Item) DataCtrl.findByCode(Item.class, Integer.valueOf(data[2]));
                    if (item == null) {
                        item = new Item(data[1], Integer.valueOf(data[2]), data[5], parent);
                    } else {
                        item.setName(data[1]);
                        item.setCode(Integer.valueOf(data[2]));
                        item.setCategory(parent);
                        item.setUnit(data[5]);
                    }
                    LOG.finest(str);
                    LOG.finest(item.toString());
                    items.add(item);
                }

            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            return err;
        }
        LOG.info("Strings in post message " + counter);
        LOG.info(items.size() + " to be saved");
        DataCtrl.persistList(items);
        LOG.info("Done");
        return err;
    }
    private static final Logger LOG = Logger.getLogger(ParserStructure.class.getName());

    public static String parseCategory(BufferedReader reader) throws IOException {
        String str = null;
        String err = null;
        int counter = 0;

        try {
            while ((str = reader.readLine()) != null) {
                String[] data = str.split(dl, -1);
                ++counter;
                Category parent = null;
                if (!data[4].isEmpty()) {
                    parent = (Category) DataCtrl.findByCode(Category.class, Integer.valueOf(data[4]));
                    if (parent == null) {
                        parent = new Category(data[3], Integer.valueOf(data[4]));
                        parent = (Category) DataCtrl.checkObject(parent);
                    }
                }

                if (data[0].equals("1")) {
                    Category category = (Category) DataCtrl.findByCode(Category.class, Integer.valueOf(data[2]));
                    if (category == null) {
                        category = new Category(data[1], Integer.valueOf(data[2]), parent);
                    } else {
                        category.setName(data[1]);
                        category.setCode(Integer.valueOf(data[2]));
                        category.setParent(parent);
                    }
                    DataCtrl.checkObject(category);
                }
            }
        } catch (NumberFormatException nfe) {
            err = "Number format error in string " + counter + "\n" + str;
            return err;
        }
        LOG.info("Strings in post message " + counter);

        return err;
    }

}
