/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

public class ParserConfig {

    public final static int sheet = 0;
    public final static int dateRow = 7;
    public final static int dateCell = 2;
    public final static int rowOffset = 11;
    public final static int codeCell = 1;
    public final static int nameCell = 2;
    public final static int priceCell = 3;
    public final static int unitCell = 4;
    public static String dl = java.util.regex.Pattern.quote("|");

}
