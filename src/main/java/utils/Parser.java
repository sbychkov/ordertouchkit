/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import utils.XLSPriceList.Node;

/**
 *
 * Reads xls and returns pricelist as PriceList object or as json
 *
 * @author serge
 */
public class Parser {

    private XLSPriceList priceList;
    private String error;

    public Parser(XLSPriceList priceList) {
        this.priceList = priceList;

    }

    public boolean getFile(String path) {
        boolean result = false;
        try {
            parse(WorkbookFactory.create(new File(path)));
            result = true;
        } catch (IOException ex) {
            error = ex.getLocalizedMessage();
        } catch (InvalidFormatException ex) {
            error = ex.getLocalizedMessage();
        }

        return result;
    }

    public boolean getInputStream(InputStream is) {
        boolean result = false;
        try {
            parse(WorkbookFactory.create(is));
            result = true;
        } catch (IOException ex) {
            error = ex.getLocalizedMessage();
        } catch (InvalidFormatException ex) {
            error = ex.getLocalizedMessage();
        }
        return result;
    }

    private void parse(Workbook wb) {
        Sheet sheet = wb.getSheetAt(ParserConfig.sheet);
        Row row = sheet.getRow(ParserConfig.dateRow);
        Cell cell = row.getCell(ParserConfig.dateCell);
        priceList.setDate(dateParse(cellParse(cell)));

        int rowStart = ParserConfig.rowOffset;
        int rowEnd = sheet.getLastRowNum();

        XLSPriceList.Node parent = null;
        XLSPriceList.Node prev = null;
        for (int rowNum = rowStart; rowNum < rowEnd; rowNum++) {
            row = sheet.getRow(rowNum);

            Object[] data = new Object[5];

            cell = row.getCell(ParserConfig.codeCell, Row.RETURN_BLANK_AS_NULL);
            data[0] = cellParse(cell);
            cell = row.getCell(ParserConfig.priceCell, Row.RETURN_BLANK_AS_NULL);
            data[2] = cellParse(cell);
            cell = row.getCell(ParserConfig.unitCell, Row.RETURN_BLANK_AS_NULL);
            data[3] = cellParse(cell);
            cell = row.getCell(ParserConfig.nameCell, Row.RETURN_BLANK_AS_NULL);
            data[1] = cellParse(cell);
            short color = cell.getCellStyle().getFillForegroundColor();
            data[4] = color;
            if (cell != null) {

                if (color == HSSFColor.BLACK.index) {
                    XLSPriceList.Node node = priceList.new Node(null, null, (String) data[1]);
                    priceList.getNodeList().add(node);
                    parent = node;
                    prev = node;
                } else if (color != HSSFColor.AUTOMATIC.index) {
                    if (prev != null && parent != null) {

                        while (prev.getParent() != null
                                && color < ((Short) ((Object[]) prev.getContent())[4])) {
                            prev = prev.getParent();
                        }
                        parent = prev.getParent();

                    }
                    XLSPriceList.Node node = priceList.new Node(parent, null, (String) data[1]);
                    parent = node;
                    prev = node;
                } else {
                    XLSPriceList.Node node = priceList.new Node(parent, data, null);
                    prev = node;
                }

            }

        }
    }

    private String cellParse(Cell cell) {
        String result = null;
        if (cell != null) {
            if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                result = ((Long) ((Double) cell.getNumericCellValue()).longValue()).toString();
            } else {
                result = cell.getStringCellValue();
            }
        }
        return result;
    }

    private Date dateParse(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        Date parse = new Date();
        try {
            parse = simpleDateFormat.parse(str);

        } catch (ParseException ex) {
            Logger.getLogger(Parser.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return parse;
    }

    public String toJSON() {
        ;
        String result = "";

        StringBuilder sb = new StringBuilder("{\n");
        sb.append("\"date\" : \"").append(priceList.getDate()).append("\",\n");
        sb.append(nodeListToJSON(priceList.getNodeList()));
        sb.append("\n}");

        return sb.toString();

    }

    private StringBuilder nodeListToJSON(List<XLSPriceList.Node> l) {
        StringBuilder sb = new StringBuilder();
        sb.append("\"list\" : ");
        sb.append("[\n");
        for (XLSPriceList.Node n : l) {
            if (!n.isLeaf()) {
                sb.append("{");
                sb.append("\"name\" : \"").append(n.getName()).append("\"");
                nodeListToJSON(n.getChildren());
                sb.append("},\n");
            } else {
                Object[] o = (Object[]) n.getContent();
                sb.append("\"code\" : \"").append(o[0]).append("\",\n");
                sb.append("\"name\" : \"").append(o[0]).append("\",\n");
                sb.append("\"price\" : \"").append(o[0]).append("\",\n");
                sb.append("\"unit\" : \"").append(o[0]).append("\"\n");
            }
        }
        sb.append("]");
        return sb;
    }

}
