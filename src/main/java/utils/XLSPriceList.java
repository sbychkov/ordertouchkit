/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class XLSPriceList implements Serializable{
    public static final String PROP_DATE = "PROP_DATE";
    public static final String PROP_NODELIST = "PROP_NODELIST";

    private Date date;
    private List<Node> nodeList;
    private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);

    public XLSPriceList() {
        date= new Date();
        nodeList = new ArrayList<Node>();                       
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        java.util.Date oldDate = this.date;
        this.date = date;
        propertyChangeSupport.firePropertyChange(PROP_DATE, oldDate, date);
    }

    /**
     * @return the nodeList
     */
    public List<Node> getNodeList() {
        return nodeList;
    }

    /**
     * @param nodeList the nodeList to set
     */
    public void setNodeList(List<Node> nodeList) {
        List<Node> oldNodeList = this.nodeList;
        this.nodeList = nodeList;
        propertyChangeSupport.firePropertyChange(PROP_NODELIST, oldNodeList, nodeList);
    }
    
    public final class Node {

        public static final String PROP_LEAF = "PROP_LEAF";
        public static final String PROP_PARENT = "PROP_PARENT";
        public static final String PROP_CONTENT = "PROP_CONTENT";
        public static final String PROP_NAME = "PROP_NAME";        
        private boolean leaf = true;
        private Node parent = null;
        private Object content = null;
        private String name = null;        
        private final transient PropertyChangeSupport propertyChangeSupport = new java.beans.PropertyChangeSupport(this);
        private ArrayList<Node> children= new ArrayList<Node>();
        
        Node() {
        }
        
        Node(Node parent) {
            setParent(parent);
        }
        
        Node(Node parent, Object content) {
            setParent(parent);
            setContent(content);
        }
        
        Node(Node parent, Object content, String name) {
            setParent(parent);
            setName(name);
            setContent(content);
        }

        /**
         * @return the parent
         */
        public Node getParent() {
            return parent;
        }

        /**
         * @param parent the parent to set
         */
        public void setParent(Node parent) {
            if (parent != null) {
                parent.setLeaf(false);
                parent.addChild(this);
            }
            Node oldParent = this.parent;
            this.parent = parent;
            propertyChangeSupport.firePropertyChange(PROP_PARENT, oldParent, parent);
        }

        /**
         * @return the content
         */
        public Object getContent() {
            return content;
        }

        /**
         * @param content the content to set
         */
        public void setContent(Object content) {
            java.lang.Object oldContent = this.content;
            this.content = content;
            propertyChangeSupport.firePropertyChange(PROP_CONTENT, oldContent, content);
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            java.lang.String oldName = this.name;
            this.name = name;
            propertyChangeSupport.firePropertyChange(PROP_NAME, oldName, name);
        }

        /**
         * @return the leaf
         */
        public boolean isLeaf() {
            return leaf;
        }

        /**
         * @param leaf the leaf to set
         */
        public void setLeaf(boolean leaf) {
            boolean oldLeaf = this.leaf;
            this.leaf = leaf;
            propertyChangeSupport.firePropertyChange(PROP_LEAF, oldLeaf, leaf);
        }
        public void addChild(Node node){
            children.add(node);
        }
        
        public List getChildren(){
            return children;
        }
    }
}
