package orderstouchkit;

import com.vaadin.addon.responsive.Responsive;
import com.vaadin.addon.touchkit.annotations.CacheManifestEnabled;
import com.vaadin.addon.touchkit.annotations.OfflineModeEnabled;
import com.vaadin.addon.touchkit.extensions.OfflineMode;
import com.vaadin.addon.touchkit.ui.NavigationManager;
import com.vaadin.addon.touchkit.ui.TabBarView;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import orderstouchkit.gwt.client.ordersPersistToServerRpc;
import orderstouchkit.ui.MapView;
import orderstouchkit.ui.MenuView;
import orderstouchkit.ui.PriceListView;
import orderstouchkit.ui.SettingsView;

/**
 * The UI's "main" class
 */
@SuppressWarnings("serial")
@Widgetset("orderstouchkit.gwt.ordersWidgetSet")
@Theme("touchkit")
// Cache static application files so as the application can be started
// and run even when the network is down.
@CacheManifestEnabled
// Switch to the OfflineMode client UI when the server is unreachable
@OfflineModeEnabled
// Make the server retain UI state whenever the browser reloads the app
@PreserveOnRefresh
public class ordersTouchKitUI extends UI {

    public static ordersTouchKitUI getApp() {
        return (ordersTouchKitUI) UI.getCurrent();
    }

    private NavigationManager navigationManager;
    private MenuView menuView;
    private final ordersPersistToServerRpc serverRpc = new ordersPersistToServerRpc() {
        @Override
        public void persistToServer() {
            // TODO this method is called from client side to store offline data
        }
    };

    @Override
    protected void init(VaadinRequest request) {

        //setStyleName(Reindeer.LAYOUT_BLUE);
        final TabBarView tabBarView = new TabBarView();
        navigationManager = new NavigationManager();
        navigationManager.setCaption("Заказы");
    
        menuView = new MenuView();
        navigationManager.setCurrentComponent(menuView);
        navigationManager.setMaintainBreadcrumb(true);
        Tab tab;
        tab = tabBarView.addTab(navigationManager);
        tab.setIcon(FontAwesome.SHOPPING_CART);        
        //tab = tabBarView.addTab(new Stock(), "Склад");
        //tab.setIcon(FontAwesome.STACK_EXCHANGE);
        tab = tabBarView.addTab(new PriceListView(), "Прайс-лист");
        tab.setIcon(FontAwesome.TABLE);
        tab = tabBarView.addTab(new MapView(), "Карта");
        tab.setIcon(FontAwesome.GLOBE);
        tab = tabBarView.addTab(new SettingsView(), "Настройки");
        tab.setIcon(FontAwesome.WRENCH);
        tabBarView.addComponentAttachListener(new ComponentAttachListener() {

            @Override
            public void componentAttachedToContainer(ComponentAttachEvent event) {
                Component attachedComponent = event.getAttachedComponent();
           if(attachedComponent.equals(navigationManager)){
               navigationManager.setCurrentComponent(menuView);
           }
            }
        });
        setContent(tabBarView);
        new Responsive(this);
        // Use of the OfflineMode connector is optional.
        OfflineMode offlineMode = new OfflineMode();
        offlineMode.extend(this);
        // Maintain the session when the browser app closes.
        offlineMode.setPersistentSessionCookie(true);
        // Define the timeout in secs to wait when a server request is sent
        // before falling back to offline mode.
        offlineMode.setOfflineModeTimeout(15);

    }

    /**
     * @return the navigationManager
     */
    public NavigationManager getNavigationManager() {
        return navigationManager;
    }

    /**
     * @param navigationManager the navigationManager to set
     */
    public void setNavigationManager(NavigationManager navigationManager) {
        this.navigationManager = navigationManager;
    }
}
