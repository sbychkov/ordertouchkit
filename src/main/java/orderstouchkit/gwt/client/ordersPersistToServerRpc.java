package orderstouchkit.gwt.client;

import com.vaadin.shared.communication.ServerRpc;

public interface ordersPersistToServerRpc extends ServerRpc {
    void persistToServer();
}
