package orderstouchkit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import com.vaadin.addon.touchkit.server.TouchKitServlet;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import java.io.IOException;
import java.util.logging.LogManager;

@SuppressWarnings("serial")
@WebServlet("/*")
public class ordersServlet extends TouchKitServlet {

    private ordersUIProvider uiProvider = new ordersUIProvider();

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        /*try {
         LogManager.getLogManager().readConfiguration(
         getServletContext().getResourceAsStream("/logging.properties"));
         } catch (IOException e) {
         System.err.println("Could not setup logger configuration: " + e.toString());
         }*/
        getService().addSessionInitListener(new SessionInitListener() {
            @Override
            public void sessionInit(SessionInitEvent event) throws ServiceException {
                event.getSession().addUIProvider(uiProvider);
            }
        });
    }

}
