package orderstouchkit;

import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import model.User;
import model.UserSettings;

@SuppressWarnings("serial")
public class ordersUIProvider extends UIProvider {

    @Override
    public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {

        boolean mobileUserAgent = event.getRequest().getHeader("user-agent")
                .toLowerCase().contains("mobile");
        boolean mobileParameter = event.getRequest().getParameter("mobile") != null;

        Object adminParameter = VaadinSession.getCurrent().getAttribute("admin");
        if (adminParameter != null && ((Boolean) adminParameter)) {
            return orderstouchkit.admin.AdminConsoleUI.class;
        }

        if (overrideMobileUA() || mobileUserAgent || mobileParameter) {
            //TODO replace with auth
            VaadinSession.getCurrent().setAttribute(User.class, new User(1L, "test", "test"));
            VaadinSession.getCurrent().setAttribute("set",
                    new UserSettings(VaadinSession.getCurrent().getAttribute(User.class)));
            
            return ordersTouchKitUI.class;
        } else {
            return ordersFallbackUI.class;
        }
    }

    private boolean overrideMobileUA() {
        VaadinSession session = VaadinSession.getCurrent();
        return session != null && session.getAttribute("mobile") != null;
    }
}
