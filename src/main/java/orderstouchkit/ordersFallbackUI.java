package orderstouchkit;

import com.vaadin.addon.touchkit.annotations.OfflineModeEnabled;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;
import fi.jasoft.qrcode.QRCode;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

@OfflineModeEnabled(false)
public class ordersFallbackUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
        getSession().setAttribute("mobile", true);
        getUI().getPage().reload();
        /*        setStyleName(Reindeer.LAYOUT_BLUE);
         VerticalLayout layout = new VerticalLayout();
         layout.setSpacing(true);
         layout.setMargin(true);
        
         Button button = new Button("Continue anyway.", new Button.ClickListener() {
         @Override
         public void buttonClick(Button.ClickEvent event) {
         getSession().setAttribute("mobile", true);
         getUI().getPage().reload();
         }
         });
         button.addStyleName("link");
        
         layout.addComponent(new Label("You seem to be using a desktop browser."));
         layout.addComponent(button);
        
         Button button2 = new Button("Admin console", new Button.ClickListener() {
        
         @Override
         public void buttonClick(Button.ClickEvent event) {
         {
         getSession().setAttribute("admin", true);
         getUI().getPage().reload();
         }
         }
         });
         layout.addComponent(button2);
         //
         try{
         URL appUrl = Page.getCurrent().getLocation().toURL();
         String myIp = Inet4Address.getLocalHost().getHostAddress();
         String qrCodeUrl = appUrl.toString().replaceAll("localhost", myIp);
        
         QRCode qrCode = new QRCode(
         "<p>Похоже вы запустили это приложение не на мобильном устройстве.\r\n "
         + "Возможна некоректная работа при отсутсвии сенсорного дисплея.\r\n "
         + "Пожалуйста считайте QR-код  вашим мобильным устройством.\r\n<p>",
         qrCodeUrl);
         qrCode.setWidth("150px");
         qrCode.setHeight("150px");
         setContent(qrCode);
         }   catch (MalformedURLException ex) {
         Logger.getLogger(ordersFallbackUI.class.getName()).log(Level.SEVERE, null, ex);
         } catch (UnknownHostException ex) {
         Logger.getLogger(ordersFallbackUI.class.getName()).log(Level.SEVERE, null, ex);
         }
         //
        
         setContent(layout);*/
        // showNonMobileNotification();
    }

    private void showNonMobileNotification() {
        try {
            URL appUrl = Page.getCurrent().getLocation().toURL();
            String myIp = Inet4Address.getLocalHost().getHostAddress();
            String qrCodeUrl = appUrl.toString().replaceAll("localhost", myIp);

            QRCode qrCode = new QRCode(
                    "<p>Похоже вы запустили это приложение не на мобильном устройстве.\r\n "
                    + "Возможна некоректная работа при отсутсвии сенсорного дисплея.\r\n "
                    + "Пожалуйста считайте QR-код  вашим мобильным устройством.\r\n<p>",
                    qrCodeUrl);
            qrCode.setWidth("150px");
            qrCode.setHeight("150px");

            CssLayout qrCodeLayout = new CssLayout(qrCode);
            qrCodeLayout.setSizeFull();

            Window window = new Window(null, qrCodeLayout);
            window.setWidth(500.0f, Unit.PIXELS);
            window.setHeight(200.0f, Unit.PIXELS);
            window.addStyleName("qr-code");

            window.setModal(true);
            window.setResizable(false);
            window.setDraggable(false);
            addWindow(window);
            window.center();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
