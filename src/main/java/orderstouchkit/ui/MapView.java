/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.touchkit.extensions.Geolocator;
import com.vaadin.addon.touchkit.extensions.PositionCallback;
import com.vaadin.addon.touchkit.gwt.client.vcom.Position;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import model.Client;
import model.Location;
import model.clientContainer;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LMarker;
import org.vaadin.addon.leaflet.LOpenStreetMapLayer;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.LeafletClickEvent;
import org.vaadin.addon.leaflet.LeafletClickListener;
import org.vaadin.addon.leaflet.shared.Point;

/**
 *
 * @author 1
 */
public class MapView extends CssLayout implements PositionCallback, LeafletClickListener {

    private double latestLatitude;
    private double latestLongitude;
    private Point center;
    //private Bounds extent;
    private LMap map;
    private Button locatebutton;
    private final LMarker you = new LMarker();

    @Override
    public void attach() {
        super.attach();
        if (map == null) {
            buildView();
        }
        updateMarkers();
      
    }

    ;
    
    private void buildView() {
        setCaption("Карта");
        map = new LMap();
        addComponent(map);

        addStyleName("mapview");
        setSizeFull();

        // Note, if you wish to use Mapbox base maps, get your own API key.
        //LTileLayer mapBoxTiles = new LTileLayer("http://{s}.tiles.mapbox.com/v3/vaadin.i1pikm9o/{z}/{x}/{y}.png");
        // map.addLayer(mapBoxTiles);
        map.setAttributionPrefix("Powered by <a href=\"leafletjs.com\">Leaflet</a> — &copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors");

        LTileLayer layer = new LOpenStreetMapLayer();
        map.addLayer(layer);
        map.setImmediate(true);

        map.setSizeFull();
        map.setZoomLevel(12);
        addComponent(map);

        // Default to Vaadin HQ
        latestLatitude = 56.52;
        latestLongitude = 35.01;
        you.setPoint(new Point(56.822990, 35.881580));
        setCenter();

        locatebutton = new Button("", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                Geolocator.detect(MapView.this);
            }
        });
        locatebutton.addStyleName("locatebutton");
        locatebutton.setWidth(30, Unit.PIXELS);
        locatebutton.setHeight(30, Unit.PIXELS);
        locatebutton.setDisableOnClick(true);
        addComponent(locatebutton);

      // Geolocator.detect(this);
    }

    @Override
    public void onFailure(final int errorCode) {
        Notification
                .show("Geolocation request failed. You must grant access for geolocation requests.",
                        Type.ERROR_MESSAGE);
    }

    @Override
    public void onClick(LeafletClickEvent event) {

        Notification
                .show("Пока не придумал зачем это надо",
                        Type.HUMANIZED_MESSAGE);

    }

    @Override
    public void onSuccess(Position position) {
        // Store the new position in the map view
        latestLatitude = position.getLatitude();
        latestLongitude = position.getLongitude();
        Notification
                .show(" " + position.toString(),
                        Type.HUMANIZED_MESSAGE);
        // Store the position also in the UI for other views
        //
        setCenter();
    }

    public final void updateMarkers() {
        List<Client> clients;
        clients = (new clientContainer(Client.class)).getItemIds();

        Iterator<Component> iterator = map.iterator();
        Collection<Component> remove = new ArrayList<Component>();
        while (iterator.hasNext()) {
            Component next = iterator.next();
            if (next instanceof LMarker) {
                remove.add(next);
            }
        }
        for (Component component : remove) {
            map.removeComponent(component);
        }

        you.setPoint(new Point(latestLatitude,
                latestLongitude));
        if (you.getParent() == null) {
            map.addComponent(you);
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        for (Client client : clients) {
            if (client.getTimeStamp().after(cal.getTime())) {
                Location location = client.getLocation();

                LMarker leafletMarker = new LMarker(location.getLatitude(),
                        location.getLongitude());
                leafletMarker.setIcon(FontAwesome.MAP_MARKER);
                leafletMarker.setIconSize(new Point(24, 38));
                leafletMarker.setIconAnchor(new Point(11, 38));
                leafletMarker.setData(client);
                leafletMarker.addClickListener(this);

                map.addComponent(leafletMarker);
            }
        }
    }

    private void setCenter() {
        if (map != null) {
            center = new Point(latestLatitude, latestLongitude);
            //extent = new Bounds(center);
            map.setCenter(center);
        }
    }

}
