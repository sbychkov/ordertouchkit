/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.NumberField;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Property;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import java.math.BigDecimal;
import model.Item;
import model.OrderElement;
import model.PriceListElement;
import model.UserSettings;
import orderstouchkit.ctrl.Containers;
import orderstouchkit.ui.widget.PriceListTree;
import utils.DataCtrl;

@SuppressWarnings("serial")
public class AddItemView extends NavigationView {

    private static final long serialVersionUID = 1L;

    private OrderElement result;
    private Item item;
    private ListSelect ls;
    private Label price;
    private Label sum;
    private NumberField quant;
    private Table table;
    private PriceListTree plt;

    @Override
    public void attach() {
        super.attach();
        //removeAllComponents();
        UserSettings us = (UserSettings) VaadinSession.getCurrent().getAttribute("set");

        VerticalLayout main = new VerticalLayout();//("Выберете товар");
        main.setSizeFull();
        setContent(main);

        VerticalComponentGroup footer = new VerticalComponentGroup();
        if (us.isPriceSelectList()) {
            ls = new ListSelect("", Containers.getItems());
            ls.setItemCaptionPropertyId("name");
            ls.addValueChangeListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    recount();
                }
            });
            main.addComponent(ls);
        } else {

            plt = new PriceListTree();
            plt.setSelectable(true);
            plt.setItemSelect(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    PriceListElement entity = (PriceListElement) event.getButton().getData();
                    item = entity.getItem();
                    price.setValue(entity.getPrice().toPlainString());
                    sum.setValue(entity.getPrice()
                            .multiply(new BigDecimal(quant.getValue())).toPlainString());
                }
            });
            plt.setSizeFull();
            //plt.setHeight("70%");            
        }
        quant = new NumberField("Количество", "0");
        price = new Label("0");
        price.setCaption("Цена");

        sum = new Label("0");
        sum.setCaption("Сумма");

        Button ok = new Button("OK", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (getItem() != null) {
                    result = new OrderElement(getQuant(), getItem(), null);
                    result.setElementPrice(new BigDecimal(price.getValue()));
                    result.setElementSum(new BigDecimal(sum.getValue()));

                }
                AddItemView.this.detach();
                result = null;
                getNavigationManager().navigateBack();
            }
        });

        ok.setWidth("100%");

        quant.addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (ls != null) {
                    recount();
                } else if (table != null) {
                    recount_table();
                }
            }
        });

        footer.addComponent(quant, 0);
        footer.addComponent(price, 1);
        footer.addComponent(sum, 2);
        footer.addComponent(ok, 3);

        main.addComponent(plt);
        main.addComponent(footer);

        main.setExpandRatio(plt, 1);

        //To change body of generated methods, choose Tools | Templates.        
    }

    private void recount() {
        Object lsId = ls.getValue();
        if (lsId != null) {
            item = ((JPAContainerItem<Item>) ls.getItem(lsId)).getEntity();
            BigDecimal itemPrice = DataCtrl.getCurrentPrice(item);
            price.setValue(itemPrice.toPlainString());
            sum.setValue(itemPrice
                    .multiply(new BigDecimal(quant.getValue())).toPlainString());
        }
    }

    private void recount_table() {
        Object tableId = table.getValue();
        if (tableId != null) {
            PriceListElement entity;
            entity = ((JPAContainerItem<PriceListElement>) table
                    .getItem(tableId)).getEntity();
            item = entity.getItem();
            price.setValue(entity.getPrice().toPlainString());
            sum.setValue(entity.getPrice()
                    .multiply(new BigDecimal(quant.getValue())).toPlainString());
        }
    }

    /**
     * @return the result
     */
    public OrderElement getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(OrderElement result) {
        this.result = result;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the ls
     */
    public ListSelect getLs() {
        return ls;
    }

    /**
     * @param ls the ls to set
     */
    public void setLs(ListSelect ls) {
        this.ls = ls;
    }

    /**
     * @return the price
     */
    public Label getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Label price) {
        this.price = price;
    }

    /**
     * @return the sum
     */
    public Label getSum() {
        return sum;
    }

    /**
     * @param sum the sum to set
     */
    public void setSum(Label sum) {
        this.sum = sum;
    }

    /**
     * @return the quant
     */
    public Integer getQuant() {
        return Integer.parseInt(quant.getValue());
    }

    /**
     * @param quant the quant to set
     */
    public void setQuant(Integer quant) {
        this.quant.setValue(quant.toString());
    }

}
