/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.touchkit.ui.HorizontalButtonGroup;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.ClientOrder;
import model.Item;
import model.OrderElement;
import org.vaadin.maddon.fields.MTable;
import utils.DataCtrl;

public class ItemOrderWidget extends Panel {

    private static final long serialVersionUID = 1L;
    private MTable table;
    private ClientOrder co;
    private boolean readOnly;
    private Button add;
    private Button del;

    public ItemOrderWidget() {
        super();
        setClientOrder(new ClientOrder());
    }

    public ItemOrderWidget(ClientOrder co) {
        super();
        setClientOrder(co);
    }

    @Override
    public void attach() {
         super.attach();
         VerticalComponentGroup layout = new VerticalComponentGroup();
        layout.addComponent(setupTable());
        layout.addComponent(addDelButtons());
        setContent(layout);
       
    }

    private MTable setupTable() {

        table = new MTable();
        table.setSelectable(true);
        table.setRowHeaderMode(Table.RowHeaderMode.INDEX);
        table.addContainerProperty("item", Item.class, new Item());
        table.addContainerProperty("quantity", Integer.class, 0);
        table.addContainerProperty("price", BigDecimal.class, BigDecimal.ZERO);
        table.addContainerProperty("sum", BigDecimal.class, BigDecimal.ZERO);

        table.setColumnHeader("item", "Товар");
        table.setColumnHeader("quantity", "Кол-во");
        table.setColumnHeader("price", "Цена");
        table.setColumnHeader("sum", "Сумма");
        table.setFooterVisible(true);
        table.setColumnFooter("item", "Итого");
        table.setColumnFooter("quantity", countQuantity().toString());
        table.setColumnFooter("sum", countSum().toPlainString());
        table.setColumnExpandRatio("item", 4);
        table.setPageLength(5);

        for (OrderElement oe : co.getElements()) {
            setRow(oe);
        }

        return table;
    }

    private void recount() {
        table.setColumnFooter("quantity", countQuantity().toString());
        table.setColumnFooter("sum", countSum().toPlainString());
    }

    private Integer countQuantity() {
        Integer result = 0;
        for (Object id : table.getItemIds()) {
            result += (Integer) table.getItem(id).getItemProperty("quantity").getValue();
        }
        return result;
    }

    private BigDecimal countSum() {
        BigDecimal result = BigDecimal.ZERO;
        for (Object id : table.getItemIds()) {
            result = result.add((BigDecimal) table.getItem(id).getItemProperty("sum").getValue());
        }
        return result;
    }

    private HorizontalButtonGroup addDelButtons() {
        HorizontalButtonGroup layout = new HorizontalButtonGroup();

        add = new Button("Добавить", FontAwesome.PLUS_SQUARE);
        add.setEnabled(!readOnly);
        add.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                final AddItemView w = new AddItemView();
                w.addDetachListener(new DetachListener() {

                    @Override
                    public void detach(DetachEvent event) {
                        OrderElement oe = w.getResult();
                        if (oe != null) {
                            setRow(oe);
                        }
                    }
                });
                orderstouchkit.ordersTouchKitUI.getApp().getNavigationManager().navigateTo(w);
            }
        });

        del = new Button("Удалить", FontAwesome.MINUS_SQUARE);
        del.setEnabled(!readOnly);
        del.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                table.removeItem(table.getValue());
            }
        });

        layout.addComponent(add);
        layout.addComponent(del);

        return layout;
    }

    public List<OrderElement> getElementsList() {
        List<OrderElement> l = new ArrayList<OrderElement>();
        Collection<?> itemIds = table.getItemIds();
        for (Object id : itemIds) {
            OrderElement ol = new OrderElement();
            com.vaadin.data.Item item = table.getItem(id);
            ol.setItem((Item) item.getItemProperty("item").getValue());
            ol.setQuantity((Integer) item.getItemProperty("quantity").getValue());
            ol.setClientOrder(co);
            l.add(ol);
        }
        return l;
    }

    private void setProp(Object id, String fieldName, Object value) {
        table.getItem(id).getItemProperty(fieldName).setValue(value);
    }

    private void setRow(OrderElement oe) {
        Object rowId = table.addItem();
        if (rowId != null) {

            setProp(rowId, "item", oe.getItem());
            setProp(rowId, "quantity", oe.getQuantity());
            setProp(rowId, "price", DataCtrl.getCurrentPrice(oe.getItem()));
            setProp(rowId, "sum", DataCtrl.getCurrentPrice(oe.getItem()).multiply(BigDecimal.valueOf(oe.getQuantity())));
        }
        recount();
    }

    private void setClientOrder(ClientOrder co) {

        this.co = co;
    }

    public ClientOrder getClientOrder() {
        return co;
    }

    /**
     * @return the readOnly
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * @param readOnly the readOnly to set
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
        if (add != null) {
            add.setEnabled(!readOnly);
        }
        if (add != null) {
            del.setEnabled(!readOnly);
        }
    }

}
