/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.data.util.filter.Compare;
import model.Client;
import model.DebtDoc;
import orderstouchkit.ctrl.Containers;
import org.vaadin.maddon.fields.MTable;

/**
 *
 * @author serge
 */
public class DebtView extends NavigationView {

    private Client client;
    private MTable table;

    public DebtView(Client client) {
        this.client = client;
    }

    @Override
    public void attach() {
        super.attach();
        table = new MTable<DebtDoc>();
        JPAContainer<DebtDoc> debts = Containers.getDebts();
        debts.addContainerFilter(new Compare.Equal("client", client));

        table.setContainerDataSource(debts);
        table.setVisibleColumns(new Object[]{"name", "docDate", "debtValue"});
        table.setColumnHeader("name", "Наименование");
        table.setColumnHeader("docDate", "Дата");
        table.setColumnHeader("debtValue", "Сумма");
        table.setWidth("100%");
        setContent(table);
        //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

}
