/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.NavigationButton;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import java.util.Collection;
import model.ClientOrder;
import model.User;
import orderstouchkit.ctrl.Containers;

/**
 *
 * @author 1
 */
@SuppressWarnings("serial")
public class OrdersView extends NavigationView {

    public OrdersView() {
        setCaption("Заказы");
        final VerticalComponentGroup content = new VerticalComponentGroup();
        JPAContainer<ClientOrder> c = Containers.getOrders();
        c.addContainerFilter(new Compare.Equal("user", VaadinSession.getCurrent().getAttribute(User.class)));
        Collection l = c.getItemIds();
        if (l == null || l.isEmpty()) {
            content.addComponent(new Label("Нет заказов"));
        } else {
            for (Object id : l) {
                final ClientOrder entity = c.getItem(id).getEntity();
                final NavigationButton button = new NavigationButton(entity.getClient().getName()
                        + " " + entity.getCreateDate().toString() + " " + entity.getSum().toPlainString());
                button.setData(entity);
                button.addClickListener(new NavigationButton.NavigationButtonClickListener() {

                    @Override
                    public void buttonClick(NavigationButton.NavigationButtonClickEvent event) {
                        OrdersView.this.getNavigationManager().navigateTo(new ClientOrderView((ClientOrder) button.getData()));
                    }
                });
                content.addComponent(button);
            }
        }

        final Button backButton = new Button("Назад");
        backButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                OrdersView.this.getNavigationManager().navigateBack();
            }
        });

        setContent(new CssLayout(content)
        //        , backButton)
        );
    }
}
