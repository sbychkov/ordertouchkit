/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Like;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import model.Client;
import orderstouchkit.ctrl.Containers;
import orderstouchkit.ui.widget.ClientTree;
import org.vaadin.maddon.fields.MTextField;

/**
 *
 * @author serge
 */
@SuppressWarnings("serial")
public class ClientSelectorView extends NavigationView {

    private Client client;
    private MTextField search;
    private JPAContainer<Client> cont;
    private Container.Filter searchFilter;
    private ClientTree ct;
    private ListSelect ls;
    private VerticalLayout content;
    
    public ClientSelectorView() {
    }

    public ClientSelectorView(DetachListener listner) {
        this.addDetachListener(listner);
    }

    @Override
    public void attach() {
        super.attach();
        
        content = new VerticalLayout();
        content.setSizeFull();
        search = new MTextField("Поиск");
        search.setIcon(FontAwesome.SEARCH);
        search.addValueChangeListener(new searchVCL());
        content.addComponent(search);

        cont = Containers.getClients();
        /* ls = new ListSelect(null, cont);
         ls.setHeight("80%");
         ls.setItemCaptionPropertyId("name");
         ls.setNewItemsAllowed(false);
         content.addComponent(ls);
         */
        Button ok = new Button("Ok", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (ls != null) {
                    setClient((Client) ((JPAContainerItem) ls.getItem(ls.getValue())).getEntity());
                }
                ClientSelectorView.this.detach();
                getNavigationManager().navigateBack();
            }
        });
        Button cancel = new Button("Отмена", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                setClient(null);
                ClientSelectorView.this.detach();
                getNavigationManager().navigateBack();
            }
        });

        ct = new ClientTree();
        ct.setItemSelect(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                Button b = event.getButton();

                if (b.getStyleName().contains(ValoTheme.BUTTON_BORDERLESS_COLORED)) {
                    b.removeStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
                    setClient(null);
                } else {
                    b.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
                    setClient((Client) b.getData());
                }
            }
        });
        ct.setSizeFull();
        content.addComponent(ct);
        content.setExpandRatio(ct, 1);
        content.addComponent(new HorizontalLayout(ok, cancel));
        setContent(content);
        //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    private class searchVCL implements Property.ValueChangeListener {

        @Override
        public void valueChange(Property.ValueChangeEvent event) {
            String searchString = event.getProperty().getValue().toString();
            if (searchString.isEmpty()) {
                cont.removeAllContainerFilters();
            } else {
                cont.removeContainerFilter(searchFilter);
                searchFilter = new Like("name", "%" + searchString + "%");
                cont.addContainerFilter(searchFilter);
            }
            if (ct != null) {
                ct.setSearchFilter(searchFilter);
            }
        }

    }

}
