/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.touchkit.ui.Switch;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Property;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CssLayout;
import model.UserSettings;

public class SettingsView extends CssLayout {

    private static final long serialVersionUID = 1L;

    @Override
    public void attach() {
        super.attach();
        VerticalComponentGroup content = new VerticalComponentGroup();
        final UserSettings us = (UserSettings) VaadinSession.getCurrent().getAttribute("set");
        final Switch aSwitch = new Switch("Просмотр прайслиста в виде дерева", us.isPriceViewTree());
        final Switch bSwitch = new Switch("Выбор товара ввиде списка", us.isPriceSelectList());

        aSwitch.addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                us.setPriceSelectList(aSwitch.getValue());
                ((UserSettings) VaadinSession.getCurrent().getAttribute("set"))
                        .setPriceViewTree(bSwitch.getValue());
            }
        });
        bSwitch.addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                us.setPriceViewTree(bSwitch.getValue());
                ((UserSettings) VaadinSession.getCurrent().getAttribute("set"))
                        .setPriceViewTree(bSwitch.getValue());
            }
        });
        content.addComponent(aSwitch);
        content.addComponent(bSwitch);

        addComponent(content);
    }

}
