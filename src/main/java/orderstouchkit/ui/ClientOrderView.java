/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.DatePicker;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import java.util.Date;
import model.Client;
import model.ClientOrder;
import model.User;
import orderstouchkit.ctrl.Containers;

/**
 *
 * @author 1
 */
@SuppressWarnings("serial")
public class ClientOrderView extends NavigationView {

    private ClientOrder co;
    private boolean readOnly;
    private Button clientName;
    private DatePicker dateField;
    private ItemOrderWidget iow;
    private Button submitButton;

    public ClientOrderView() {
        this(new ClientOrder());
    }

    ClientOrderView(ClientOrder entity) {
        this(entity, false);
    }

    public ClientOrderView(ClientOrder entity, boolean readOnly) {
        this(entity, null, readOnly);
    }

    public ClientOrderView(ClientOrder entity, String caption, boolean readOnly) {
        super(caption);
        if (entity != null) {
            this.co = entity;
        } else {
            co = new ClientOrder();
        }
        this.readOnly = readOnly;
        clientName = new Button();
        dateField = new DatePicker();
        submitButton = new Button();
    }

    @Override
    public void attach() {
        super.attach();
        final VerticalComponentGroup content = new VerticalComponentGroup();

        if (co.getClient() == null) {
            clientName = new Button("Выберите Клиента");
        } else {
            clientName.setCaption(co.getClient().getName());
        }
        clientName.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                final ClientSelectorView clientSelector = new ClientSelectorView();
                clientSelector.addDetachListener(new DetachListener() {

                    @Override
                    public void detach(DetachEvent event) {
                        Client client = clientSelector.getClient();
                        if (client != null) {
                            co.setClient(client);
                            clientName.setCaption(client.getName());
                        }
                    }
                });
               getNavigationManager().navigateTo(clientSelector);
            }
        });
        clientName.setWidth("100%");
        content.addComponent(clientName);

        dateField = new DatePicker("Ожидаемая дата доставки");
        content.addComponent(dateField);

        iow = new ItemOrderWidget(co);
        iow.setReadOnly(readOnly);
        iow.setCaption("Товары");
        iow.setHeight("70%");
        content.addComponent(iow);

        if (readOnly) {
            submitButton.setCaption("Сохранить");
            submitButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    co.setCreateDate(new Date());
                    co.setEstimateDate(dateField.getValue());
                    co.setElements(iow.getElementsList());
                    co.setUser(getSession().getAttribute(User.class));
                    JPAContainer<ClientOrder> orders = Containers.getOrders();

                    orders.addEntity(co);
                    Notification.show("Thanks!");
                    getNavigationManager().navigateBack();
                }
            });
        } else {
            submitButton.setCaption("Назад");
            submitButton.addClickListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    getNavigationManager().navigateBack();
                }
            });
        }

        content.addComponent(submitButton);
        setContent(content);
    }

      
    @Override
     public boolean isReadOnly() {
     return readOnly;
     }

    
    @Override
     public void setReadOnly(boolean readOnly) {
     this.readOnly = readOnly;
        
     submitButton.setEnabled(!readOnly);
     dateField.setEnabled(!readOnly);
     clientName.setEnabled(!readOnly);
     }
    
}
