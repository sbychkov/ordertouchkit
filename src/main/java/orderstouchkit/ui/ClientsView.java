/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import model.Client;
import orderstouchkit.ctrl.Containers;
import orderstouchkit.ui.widget.ClientTree;
import org.vaadin.maddon.fields.MTable;
import org.vaadin.maddon.fields.MTextField;
import org.vaadin.maddon.fields.MValueChangeEvent;
import org.vaadin.maddon.fields.MValueChangeListener;

/**
 *
 * @author 1
 */
@SuppressWarnings("serial")
public class ClientsView extends NavigationView {

    private MTextField search;
    private MTable<Client> table;
    private JPAContainer<Client> cont;
    private Filter searchFilter;
    private ClientTree ct;

    @Override
    public void attach() {

        super.attach();
        buildUi();
    }

    private void buildUi() {
        VerticalComponentGroup layout = new VerticalComponentGroup();
        layout.setSizeFull();

        search = new MTextField("Поиск");
        search.setIcon(FontAwesome.SEARCH);
        cont = Containers.getClients();
        /*table = new MTable<Client>();
         

         table.setContainerDataSource(cont);
         table.setVisibleColumns(new String[]{"id", "name", "debt"});
         table.setColumnHeader("name", "Наименование");
         table.setColumnHeader("debt", "Долг");
         table.setColumnExpandRatio("name", 7);

         table.addItemClickListener(new tableICL());
         */
        search.addValueChangeListener(new searchVCL());
        //  table.addMValueChangeListener(new tableVCL());

        layout.addComponent(search);
        //layout.addComponent(table);
        //setContent(layout);
        ct = new ClientTree();
        ct.setItemSelect(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                Client client = (Client) event.getButton().getData();
                ClientVaadinForm clientVaadinForm = new ClientVaadinForm(client);
                ClientsView.this.getNavigationManager().navigateTo(clientVaadinForm);
            }
        });
        ct.setSizeFull();
        layout.addComponent(ct);
        setContent(layout);

    }

    private class searchVCL implements Property.ValueChangeListener {

        @Override
        public void valueChange(Property.ValueChangeEvent event) {
            String searchString = event.getProperty().getValue().toString();
            if (searchString.isEmpty()) {
                cont.removeAllContainerFilters();

            } else {
                cont.removeContainerFilter(searchFilter);
                searchFilter = new Like("name", "%" + searchString + "%");
                cont.addContainerFilter(searchFilter);
            }
            if (ct != null) {
                ct.setSearchFilter(searchFilter);
            }
        }

    }

    private class tableVCL implements MValueChangeListener {

        @Override
        public void valueChange(MValueChangeEvent event) {

        }

    }

    private class tableICL implements ItemClickEvent.ItemClickListener {

        @Override
        public void itemClick(ItemClickEvent event) {

            Client client = (Client) ((EntityItem) event.getItem()).getEntity();
            ClientVaadinForm clientVaadinForm = new ClientVaadinForm(client);
            ClientsView.this.getNavigationManager().navigateTo(clientVaadinForm);
        }

    }

}
