/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui.widget;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.ValoTheme;
import model.Category;
import model.PriceListElement;
import orderstouchkit.ctrl.Containers;
import utils.DataCtrl;

/**
 *
 * @author 1
 */
public class PriceListTree extends Panel {

    private JPAContainer<Category> containerC;
    private JPAContainer<PriceListElement> containerP;
    //private Category root;
    private Category parent;
    // private Category current;
    private Filter filterC;
    private Filter filterP;
    private VerticalComponentGroup content;
    private boolean selectable;
    private ClickListener itemSelect;
    private Filter searchFilter;

    public PriceListTree() {
        content = new VerticalComponentGroup();
        content.setSizeUndefined();

        containerP = Containers.getPriceListElements();
        containerP.addContainerFilter(new Compare.Equal("priceList",
                DataCtrl.getLatestPriceList()));
        containerP.addNestedContainerProperty("item.category");
        containerC = Containers.getCategory();
        containerC.setParentProperty("parent");

        getTree();

        parent = null;
        filterC = new Compare.Equal("parent", parent);
        filterP = new Compare.Equal("item.category", parent);
        selectable = false;
    }

    @Override
    public void attach() {
        super.attach();
        setContent(content);

        //To change body of generated methods, choose Tools | Templates.
    }

    private void getTree() {
        containerC.removeContainerFilter(filterC);
        filterC = new Compare.Equal("parent", parent);
        containerC.addContainerFilter(filterC);
        if (parent != null) {
            Button buttonBack = new Button(parent.getName());
            buttonBack.setData(parent.getParent());
            buttonBack.setIcon(FontAwesome.BACKWARD);
            buttonBack.addClickListener(new click());
            content.addComponent(buttonBack);
        }
        for (Object id : containerC.getItemIds()) {
            Category item = containerC.getItem(id).getEntity();
            Button button = new Button(item.getName());
            button.setData(item);
            button.setIcon(FontAwesome.PLUS_CIRCLE);
            button.addClickListener(new click());
            content.addComponent(button);

        }

        containerP.removeContainerFilter(filterP);
        filterP = new Compare.Equal("item.category", parent);
        containerP.addContainerFilter(filterP);
        for (Object id : containerP.getItemIds()) {
            PriceListElement item = containerP.getItem(id).getEntity();
            if (selectable && itemSelect != null) {
                Button button = new Button(itemCaption(item));
                button.setHtmlContentAllowed(true);
                button.setData(item);
                button.setStyleName(ValoTheme.BUTTON_QUIET);
                button.addClickListener(itemSelect);

                content.addComponent(button);
            } else {
                Label label = new Label(itemCaption(item));

                label.setContentMode(ContentMode.HTML);

                content.addComponent(label);
            }

        }

    }

    private String itemCaption(PriceListElement item) {
        String result;
        result = "<table width=\"100%\"><tr>"
                + "<td width=\"70%\">" + item.getItem().getName() + "</td>"
                + "<td width=\"10%\">" + item.getPrice().toPlainString() + "</td>"
                + "<td width=\"10%\">" + item.getQuantity().stripTrailingZeros()
                .toPlainString() + "</td>"
                + "<td width=\"10%\">" + item.getItem().getUnit() + "</td>"
                + "</tr></table>";
        return result;
    }

    private Table getTable() {
        Table result;
        result = new Table();
        containerP.removeContainerFilter(filterP);
        containerP.addContainerFilter(filterP);
        result.setContainerDataSource(containerP);
        return result;
    }

    /**
     * @return the selectable
     */
    public boolean isSelectable() {
        return selectable;
    }

    /**
     * @param selectable the selectable to set
     */
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    /**
     * @return the itemSelect
     */
    public ClickListener getItemSelect() {
        return itemSelect;
    }

    /**
     * @param itemSelect the itemSelect to set
     */
    public void setItemSelect(ClickListener itemSelect) {
        this.itemSelect = itemSelect;
    }

    /**
     * @return the searchFilter
     */
    public Filter getSearchFilter() {
        return searchFilter;
    }

    /**
     * @param searchFilter the searchFilter to set
     */
    public void setSearchFilter(Filter searchFilter) {

        containerC.removeContainerFilter(searchFilter);
        containerP.removeContainerFilter(searchFilter);
        this.searchFilter = searchFilter;
        if (searchFilter != null) {
            containerC.addContainerFilter(searchFilter);
            containerP.addContainerFilter(searchFilter);
        }
    }

    private class click implements Button.ClickListener {

        @Override
        public void buttonClick(Button.ClickEvent event) {
            Object data = event.getButton().getData();
            parent = (Category) data;
            content.removeAllComponents();
            getTree();
        }
    }

}
