/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui.widget;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import org.vaadin.maddon.fields.MTextField;

/**
 *
 * @author 1
 */
@SuppressWarnings("serial")
public class NumberPicker extends HorizontalLayout {

    private Button plus;
    private Button plus10;
    private Button minus;
    private Button minus10;
    private MTextField field;
    private Integer value;

    public NumberPicker() {

        plus = new Button("+", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                value++;
                field.setValue(value.toString());

            }
        });
        plus10 = new Button("+10", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                value += 10;
                field.setValue(value.toString());

            }
        });
        minus = new Button("-", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                value--;
                field.setValue(value.toString());

            }
        });
        minus10 = new Button("-10", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                value -= 10;
                field.setValue(value.toString());
            }
        });

        field = new MTextField();
        field.addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                try {
                    field.validate();
                    String value1 = (String) event.getProperty().getValue();
                    value = Integer.valueOf(value1);

                    if (value < 10 && minus10.isEnabled()) {
                        minus10.setEnabled(false);
                    }
                    if (value == 0) {
                        minus.setEnabled(false);
                    }
                    if (value > 0 && !minus.isEnabled()) {
                        minus.setEnabled(true);
                    }
                    if (value > 9 && !minus10.isEnabled()) {
                        minus10.setEnabled(true);
                    }
                } catch (Validator.InvalidValueException e) {
                } catch (NumberFormatException e) {
                }
            }

        });
        field.setValidationVisible(true);
        field.addValidator(new Validator() {

            @Override
            public void validate(Object value) throws Validator.InvalidValueException {
                if (!((String) value).matches("\\d+")) {
                    Notification.show("В это поле можно вносить только числовые значения!");
                }
            }
        });
        value = 0;
        field.setValue(value.toString());

    }

    public NumberPicker(Integer value) {
        this();
        this.value = value;
    }

    public NumberPicker(String caption, Integer value) {
        this();
        this.value = value;
        this.setCaption(caption);
    }

    public NumberPicker(String caption) {
        this();
        this.setCaption(caption);
    }

    @Override
    public void attach() {
        super.attach();
        addComponent(minus10, 0);
        addComponent(minus, 1);
        addComponent(field, 2);
        addComponent(plus, 3);
        addComponent(plus10, 4);

    }

    public void addValueChangeListener(ValueChangeListener valueChangeListener) {
        field.addValueChangeListener(valueChangeListener);
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
