/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui.widget;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Container;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import model.Client;
import model.ClientGroup;
import orderstouchkit.ctrl.Containers;

/**
 *
 * @author serge
 */
@SuppressWarnings("serial")
public class ClientTree extends Panel {

    private final JPAContainer<ClientGroup> containerG;
    private final JPAContainer<Client> containerC;
    private ClientGroup parent;
    private Container.Filter filterC;
    private Container.Filter filterG;
    private final VerticalComponentGroup content;
    private Button.ClickListener itemSelect;
    private Container.Filter searchFilter;

    public ClientTree() {
        content = new VerticalComponentGroup();
        content.setSizeUndefined();
        containerC = Containers.getClients();
        containerG = Containers.getClientGroups();

        buildTree();
        parent = null;
        filterC = new Compare.Equal("parent", parent);
        //filterG=new Compare.Equal(parent, this)
    }

    @Override
    public void attach() {
         super.attach(); 
         setContent(content);
      //To change body of generated methods, choose Tools | Templates.
    }

    private void buildTree() {
        containerC.removeContainerFilter(filterC);
        containerG.removeContainerFilter(filterC);
        filterC = new Compare.Equal("parent", parent);
        containerC.addContainerFilter(filterC);
        containerG.addContainerFilter(filterC);
        if (parent != null) {
            Button buttonBack = new Button(parent.getName());
            buttonBack.setData(parent.getParent());
            buttonBack.setIcon(FontAwesome.BACKWARD);
            buttonBack.addClickListener(new click());
            content.addComponent(buttonBack);
        }

        for (Object id : containerG.getItemIds()) {
            ClientGroup item = containerG.getItem(id).getEntity();

            Button button = new Button(itemCaption(item));
            button.setIcon(FontAwesome.PLUS_CIRCLE);
            button.setData(item);
            button.addClickListener(new click());
            content.addComponent(button);
        }

        for (Object id : containerC.getItemIds()) {
            Client item = containerC.getItem(id).getEntity();
            Button button = new Button(item.getName());
            button.setData(item);
            button.setHtmlContentAllowed(true);
            if (itemSelect != null) {
                button.addClickListener(itemSelect);
            }
            content.addComponent(button);

        }

    }

    private String itemCaption(ClientGroup item) {
        String result = item.getName();
        return result;
    }

    /**
     * @return the itemSelect
     */
    public Button.ClickListener getItemSelect() {
        return itemSelect;
    }

    /**
     * @param itemSelect the itemSelect to set
     */
    public void setItemSelect(Button.ClickListener itemSelect) {
        this.itemSelect = itemSelect;
    }

    /**
     * @return the searchFilter
     */
    public Container.Filter getSearchFilter() {
        return searchFilter;
    }

    /**
     * @param searchFilter the searchFilter to set
     */
    public void setSearchFilter(Container.Filter searchFilter) {
        this.searchFilter = searchFilter;
    }

    private class click implements Button.ClickListener {

        @Override
        public void buttonClick(Button.ClickEvent event) {
            parent = (ClientGroup) event.getButton().getData();
            content.removeAllComponents();
            buildTree();
        }
    }
}
