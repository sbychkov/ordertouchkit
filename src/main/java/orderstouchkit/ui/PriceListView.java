/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Like;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.Tree;
import com.vaadin.ui.TreeTable;
import java.util.List;
import model.Category;
import model.PriceList;
import model.PriceListElement;
import model.UserSettings;
import static orderstouchkit.ctrl.Containers.getPriceListElements;
import orderstouchkit.ui.widget.PriceListTree;
import org.vaadin.maddon.fields.MTable;
import org.vaadin.maddon.fields.MTextField;
import utils.DataCtrl;

@SuppressWarnings("serial")
public class PriceListView extends CssLayout {

    private Tree tree;
    private TreeTable treeTable;
    private MTable table;
    private JPAContainer<Category> containerC;
    private JPAContainer<PriceListElement> containerP;
    private MTextField search;
    private PriceListTree plt;

    public PriceListView() {
        plt = new PriceListTree();
    }

    @Override
    public void attach() {
        super.attach();
        removeAllComponents();
        search = new MTextField("Поиск");
        search.setIcon(FontAwesome.SEARCH);
        search.addValueChangeListener(new searchVCL());

        //
        if (((UserSettings) VaadinSession.getCurrent().getAttribute("set")).isPriceViewTree()) {
            HorizontalLayout content = new HorizontalLayout();
            content.addComponent(buildTree());
            table = getTable(new Compare.Equal("item.category", null));
            content.addComponent(table);
            // content.addComponent(getTreeTable());
            addComponent(content);
        } else {
            VerticalComponentGroup content = new VerticalComponentGroup();
            //content.addComponent(getTable());
            plt = new PriceListTree();
            // content.addComponent(search);
            content.addComponent(plt);
            addComponent(content);
        }

    }

    private MTable getTable() {
        return getTable(null);
    }

    private MTable getTable(Filter f) {
        JPAContainer<PriceListElement> container = getPriceListElements();
        if (f != null) {
            container.addContainerFilter(f);
        }
        PriceList pr = DataCtrl.getLatestPriceList();
        container.addContainerFilter(new Compare.Equal("priceList", pr));

        MTable table = new MTable();

        container.addNestedContainerProperty("item.name");
        container.addNestedContainerProperty("item.unit");
        table.setContainerDataSource(container);
        table.setRowHeaderMode(Table.RowHeaderMode.INDEX);

        table.setVisibleColumns("item.name", "price", "quantity", "item.unit");
        table.setColumnHeader("item.name", "Товар");
        table.setColumnHeader("quantity", "Кол-во");
        table.setColumnHeader("price", "Цена");
        table.setColumnHeader("item.unit", "Ед. изм.");
        table.setColumnExpandRatio("id", 1);

        return table;
    }

    private Tree buildTree() {

        containerC = JPAContainerFactory.makeReadOnly(Category.class, "PU");
        containerC.setParentProperty("parent");
        tree = new Tree("Товары", containerC);
        //getNode(null);

        tree.setItemCaptionPropertyId("name");
        tree.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        tree.addExpandListener(new Tree.ExpandListener() {

            @Override
            public void nodeExpand(Tree.ExpandEvent event) {

                Category c = ((JPAContainerItem<Category>) tree.getItem(event.getItemId())).getEntity();

                JPAContainer<PriceListElement> container = getPriceListElements();
                container.addNestedContainerProperty("item.category");
                container.addNestedContainerProperty("item.category.parent");
                container.addNestedContainerProperty("item.name");
                container.addNestedContainerProperty("item.unit");
                PriceList pr = DataCtrl.getLatestPriceList();
                container.addContainerFilter(new Compare.Equal("priceList", pr));

                //table = getTable(new Compare.Equal("parent", c));
                // container.removeAllContainerFilters();
                container.addContainerFilter(new Compare.Equal("item.category", c));
                table.setContainerDataSource(container);

                table.setVisibleColumns("item.name", "price", "quantity", "item.unit");
                table.setColumnHeader("item.name", "Товар");
                table.setColumnHeader("quantity", "Кол-во");
                table.setColumnHeader("price", "Цена");
                table.setColumnHeader("item.unit", "Ед. изм.");
               // table.setColumnExpandRatio("id", 1);

                // tree.markAsDirtyRecursive();
            }
        });

        /*        tree.addCollapseListener(new Tree.CollapseListener() {

         @Override
         public void nodeCollapse(Tree.CollapseEvent event) {
         
         }
         });
         */
        return tree;
    }

    private TreeTable getTreeTable() {
        JPAContainer<PriceListElement> container = getPriceListElements();
        PriceList pr = DataCtrl.getLatestPriceList();
        container.addContainerFilter(new Compare.Equal("priceList", pr));
        container.addNestedContainerProperty("item.name");
        container.addNestedContainerProperty("item.unit");
        container.addNestedContainerProperty("item.category");
        container.addNestedContainerProperty("item.category.parent");
        container.setParentProperty("item.category.parent");

        treeTable = new TreeTable();
        treeTable.setContainerDataSource(container);
        treeTable.setVisibleColumns("item.category", "item.name", "price",
                "quantity", "item.unit");
        treeTable.setColumnHeader("item.name", "Товар");
        treeTable.setColumnHeader("quantity", "Кол-во");
        treeTable.setColumnHeader("price", "Цена");
        treeTable.setColumnHeader("item.unit", "Ед. изм.");
        treeTable.setColumnHeader("item.category", "Группа товаров");
        treeTable.setHierarchyColumn("item.category");
        //treeTable.setWidth("100%");

        return treeTable;
    }

    private void getNode(Category parent) {
        List<Category> subCategory = DataCtrl.getSubCategory(parent);
        for (Category c : subCategory) {
            tree.addItem(c);
            //tree.setItemCaptionPropertyId("name");
            //tree.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
            tree.setParent(c, parent);
            getNode(c);
        }
    }

    private class searchVCL implements Property.ValueChangeListener {

        private Like searchFilter;

        public searchVCL() {
        }

        @Override
        public void valueChange(Property.ValueChangeEvent event) {
            String searchString = event.getProperty().getValue().toString();
            if (plt != null) {
                if (searchString.isEmpty()) {
                    plt.setSearchFilter(null);
                } else {
                    searchFilter = new Like("name", "%" + searchString + "%");
                    plt.setSearchFilter(searchFilter);
                }
            }
        }
    }
}
