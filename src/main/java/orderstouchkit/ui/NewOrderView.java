package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.responsive.Responsive;
import com.vaadin.addon.touchkit.ui.DatePicker;
import com.vaadin.addon.touchkit.ui.NavigationButton;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import java.util.Date;
import model.Client;
import model.ClientOrder;
import model.User;
import orderstouchkit.ctrl.Containers;

@SuppressWarnings("serial")
public class NewOrderView extends NavigationView {
    
    ClientOrder co;
    
    public NewOrderView() {
        
        co = new ClientOrder();
        setCaption("Новый заказ");
        final VerticalComponentGroup content = new VerticalComponentGroup();
        final NavigationButton debt = new NavigationButton();
        
        
        final Button clientName = new Button("Выберете Клиента");
        clientName.addClickListener(new ClickListener() {
            
            @Override
            public void buttonClick(ClickEvent event) {
                final ClientSelectorView clientSelector = new ClientSelectorView();
                clientSelector.addDetachListener(new DetachListener() {
                    
                    @Override
                    public void detach(DetachEvent event) {
                        Client client = clientSelector.getClient();
                        if (client != null) {
                            co.setClient(client);
                            clientName.setCaption(client.getName());
                            debt.setCaption("Текущая задолженность " 
                                    + client.getDebt().toPlainString());
                            debt.setVisible(true);
                        }
                    }
                });
                getNavigationManager().navigateTo(clientSelector);
            }
        });
        clientName.setWidth("100%");
        content.addComponent(clientName);
        debt.setWidth("100%");
        debt.setVisible(false);
        debt.addClickListener(new NavigationButton.NavigationButtonClickListener() {
            
            @Override
            public void buttonClick(NavigationButton.NavigationButtonClickEvent event) {
                DebtView debtView = new DebtView(co.getClient());
                getNavigationManager().navigateTo(debtView);
            }
        });
        content.addComponent(debt);
        
        final DatePicker dateField = new DatePicker("Ожидаемая дата поставки");
        content.addComponent(dateField);
        
        final ItemOrderWidget iow = new ItemOrderWidget(co);
        iow.setCaption("Товары");
        //iow.setHeight("70%");
        Responsive responsive = new Responsive(iow);
        content.addComponent(iow);
        
        final Button submitButton = new Button("Сохранить");
        submitButton.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                co.setCreateDate(new Date());
                co.setEstimateDate(dateField.getValue());
                co.setElements(iow.getElementsList());
                co.setUser(getSession().getAttribute(User.class));
                JPAContainer<ClientOrder> orders = Containers.getOrders();
                
                orders.addEntity(co);
                Notification.show("Thanks!");
                getNavigationManager().navigateBack();
            }
        });
        
        setContent(new CssLayout(content, submitButton));
    }
    
}
