package orderstouchkit.ui;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.TextField;
import model.Client;
import model.ClientOrder;
import orderstouchkit.ctrl.Containers;
import org.vaadin.maddon.fields.MTable;
import org.vaadin.maddon.fields.MTextField;

@SuppressWarnings("serial")
public class ClientVaadinForm extends NavigationView {

    private Client client;
    private final TextField id = new MTextField("id");
    private final TextField name = new MTextField("name");
    private final MTable<ClientOrder> table = new MTable<ClientOrder>();

    public ClientVaadinForm(Client client) {
        if (client != null) {
            this.client = client;
            if (client.getId() != null) {
                id.setValue(client.getId().toString());
            }
            name.setValue(client.getName());
            JPAContainer<ClientOrder> cont = Containers.getOrders();
            table.setContainerDataSource(cont);
            cont.addContainerFilter(new Compare.Equal("client", client));
            table.setVisibleColumns(new String[]{"id", "createDate"});
            table.setColumnHeader("createDate", "Дата");
            //table.setColumnHeader("sum", "Сумма");

            table.addItemClickListener(new TableICL());

        }
    }

    @Override
    public void attach() {
        super.attach();
        VerticalComponentGroup layout = new VerticalComponentGroup();

        layout.addComponent(id);
        layout.addComponent(name);
        layout.addComponent(table);

        setContent(layout);
    }

    private class TableICL implements ItemClickEvent.ItemClickListener {

        @Override
        public void itemClick(ItemClickEvent event) {
            ClientOrder clientOrder = ((JPAContainerItem<ClientOrder>) event.getItem()).getEntity();
            ClientOrderView clientOrderView = new ClientOrderView(clientOrder);
            clientOrderView.setReadOnly(true);
            ClientVaadinForm.this.getNavigationManager().navigateTo(clientOrderView);
        }
    }

}
