package orderstouchkit.ui;

import com.vaadin.addon.touchkit.ui.NavigationButton;
import com.vaadin.addon.touchkit.ui.NavigationButton.NavigationButtonClickEvent;
import com.vaadin.addon.touchkit.ui.NavigationButton.NavigationButtonClickListener;
import com.vaadin.addon.touchkit.ui.NavigationView;
import com.vaadin.addon.touchkit.ui.VerticalComponentGroup;

@SuppressWarnings("serial")
public class MenuView extends NavigationView {

    public MenuView() {
        setCaption("Menu");

        final VerticalComponentGroup content = new VerticalComponentGroup();
        NavigationButton button1 = new NavigationButton("Новый заказ");
        button1.addClickListener(new NavigationButtonClickListener() {
            @Override
            public void buttonClick(NavigationButtonClickEvent event) {
                getNavigationManager().navigateTo(new NewOrderView());
            }
        });
        NavigationButton button2 = new NavigationButton("Мои заказы");
        button2.addClickListener(new NavigationButtonClickListener() {
            @Override
            public void buttonClick(NavigationButtonClickEvent event) {
                getNavigationManager().navigateTo(new OrdersView());
            }
        });
        NavigationButton button3 = new NavigationButton("Клиенты");
        button3.addClickListener(new NavigationButtonClickListener() {
            @Override
            public void buttonClick(NavigationButtonClickEvent event) {
                getNavigationManager().navigateTo(new ClientsView());
            }
        });
        content.addComponent(button1);
        content.addComponent(button2);
        content.addComponent(button3);
        setContent(content);
    };
}
