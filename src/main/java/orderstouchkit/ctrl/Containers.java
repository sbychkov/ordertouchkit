/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.ctrl;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Container;
import javax.persistence.EntityManagerFactory;
import model.Category;
import model.Client;
import model.ClientGroup;
import model.ClientOrder;
import model.DebtDoc;
import model.Item;
import model.PriceListElement;
import model.User;

/**
 *
 * @author 1
 */
public class Containers {

    private static final String pu="PU";

    public static JPAContainer<PriceListElement> getPriceListElements() {
        return JPAContainerFactory.makeReadOnly(PriceListElement.class, pu);
    }

    public static Containers getInstance() {
        return NewSingletonHolder.INSTANCE;
    }


    public static JPAContainer<Client> getClients() {
        return JPAContainerFactory.make(Client.class, pu);
    }

    public static JPAContainer<ClientOrder> getOrders() {
        return JPAContainerFactory.make(ClientOrder.class, pu);
    }
    public static JPAContainer<User> getUsers() {
        return JPAContainerFactory.make(User.class, pu);
    }
    
    public static JPAContainer<Item> getItems() {
        return JPAContainerFactory.make(Item.class, pu);
    }
    public static JPAContainer<DebtDoc> getDebts() {
        return JPAContainerFactory.make(DebtDoc.class, pu);
    }

    public static JPAContainer<Category> getCategory() {
        return JPAContainerFactory.make(Category.class, pu);
    }
    
     public static JPAContainer<ClientGroup> getClientGroups() {
        return JPAContainerFactory.make(ClientGroup.class, pu);
    }
    
    EntityManagerFactory emf;

    private Containers() {
    }

    private static class NewSingletonHolder {

        private static final Containers INSTANCE = new Containers();
    }

}
