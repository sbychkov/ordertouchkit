/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.admin;

import com.vaadin.ui.Button;
import com.vaadin.ui.themes.Reindeer;
import org.vaadin.maddon.layouts.MVerticalLayout;

/**
 *
 * @author serge
 */
public class SideView extends MVerticalLayout {

    public SideView() {

        setStyleName(Reindeer.LAYOUT_BLACK);
        
        Button button1 = new Button("Пользователи");
        button1.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ((AdminConsoleUI)getUI()).getNav().navigateTo("user");
            }
        });
        Button button2 = new Button("Заказы");
        button2.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ((AdminConsoleUI)getUI()).getNav().navigateTo("order");
            }
        });
        Button button3 = new Button("Активные пользователи");
        Button button4 = new Button("что-то");
        
        
        
        add(button1);
        add(button2);
        add(button3);
        add(button4);
        
    }

}
