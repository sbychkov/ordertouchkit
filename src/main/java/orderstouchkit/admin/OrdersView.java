/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.admin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Label;
import model.ClientOrder;
import orderstouchkit.ctrl.Containers;
import org.vaadin.maddon.fields.MTable;
import org.vaadin.maddon.layouts.MVerticalLayout;

/**
 *
 * @author serge
 */
public class OrdersView extends MVerticalLayout implements View {

    public OrdersView() {

        add(new Label("Заказы"));
        MTable<ClientOrder> table = new MTable<ClientOrder>();
        table.setContainerDataSource(Containers.getOrders());
        add(table);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }

}
