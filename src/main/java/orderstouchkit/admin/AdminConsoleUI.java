/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orderstouchkit.admin;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.addon.borderlayout.BorderLayout;
import org.vaadin.maddon.layouts.MHorizontalLayout;
import org.vaadin.maddon.layouts.MVerticalLayout;

/**
 *
 * @author serge
 */
public class AdminConsoleUI extends UI {

    private MHorizontalLayout header;
    private MHorizontalLayout footer;
    private MVerticalLayout side;
    private MVerticalLayout main;
    private Navigator nav;

    @Override
    protected void init(VaadinRequest request) {

        setTheme("valo-dark");
        

        BorderLayout layout = new BorderLayout();
        header = new MHorizontalLayout();
        side = new SideView();
        main = new MVerticalLayout();
        footer = new MHorizontalLayout();

        nav = new Navigator(this, main);

        nav.addView("user", UsersView.class);
        nav.addView("order", OrdersView.class);
        nav.addView("", Navigator.EmptyView.class);

        header.setWidth("100%");
        footer.setWidth("100%");
       
        
        layout.addComponent(header,BorderLayout.Constraint.NORTH);
        layout.addComponent(side,BorderLayout.Constraint.WEST);
        layout.addComponent(main, BorderLayout.Constraint.CENTER);
        layout.addComponent(footer,BorderLayout.Constraint.SOUTH);

        header.add(new Button("Выход", new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                getSession().setAttribute("admin", false);
                getUI().getPage().reload();
            }
        }));
        header.alignAll(Alignment.TOP_RIGHT);

        setContent(layout);
       // showBorders();
    }

    /**
     * @return the header
     */
    public MHorizontalLayout getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(MHorizontalLayout header) {
        this.header = header;
    }

    /**
     * @return the footer
     */
    public MHorizontalLayout getFooter() {
        return footer;
    }

    /**
     * @param footer the footer to set
     */
    public void setFooter(MHorizontalLayout footer) {
        this.footer = footer;
    }

    /**
     * @return the side
     */
    public MVerticalLayout getSide() {
        return side;
    }

    /**
     * @param side the side to set
     */
    public void setSide(MVerticalLayout side) {
        this.side = side;
    }

    /**
     * @return the main
     */
    public MVerticalLayout getMain() {
        return main;
    }

    /**
     * @param main the main to set
     */
    public void setMain(MVerticalLayout main) {
        this.main = main;
    }

    /**
     * @return the nav
     */
    public Navigator getNav() {
        return nav;
    }

    /**
     * @param nav the nav to set
     */
    public void setNav(Navigator nav) {
        this.nav = nav;
    }

    private void showBorders() {
        String style = "v-ddwrapper-over";
        setStyleName(style);
        side.setStyleName(style);
        footer.setStyleName(style);
        header.setStyleName(style);
        main.setStyleName(style);
    }
}
